<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=text], select{
   
 	
 	border-radius: 4px;
 	
 }input[type=case]{
 	width: 95%;	 
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
</style>
<title>Terrain Manager admin edit</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Terrain Manager</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
		<fieldset>
		<form:form method="GET" action="/TerrainManager/admin/home">
			<input type="submit" value="Home"/>
		</form:form>
		
		<form:form action="/TerrainManager/admin/search" method="POST">
			<form:input path="opValue" type="text" placeholder="Employee name or PNC"/>
			
			<input type="submit" name="search" value="Search"/>
			
		 </form:form>
		 		  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form:form method="GET" action="/TerrainManager/logout">Logged in: <%=session.getAttribute("name") %>  <input type="submit" value="Log out"/></form:form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<fieldset >
	<legend><h2>User table</h2></legend>
	<br>
	
	<form action="/TerrainManager/admin/userApply" method="GET">
	<% 
			HashMap<String,ArrayList<String>> table=(HashMap<String,ArrayList<String>>)request.getAttribute("table");
		if(table!=null)
		{
			
			int numRows =0;
			Set<String> keys=new HashSet<String>(table.keySet());
			String id="ID";
			if(table.containsKey("ID"))
				numRows =table.get(id).size();
			else if(table.containsKey("id"))
			{
				id="id";
				numRows =table.get(id).size();
			}
			
			keys.remove(id);
			if(!keys.contains(null))
			{
				out.println("<table border=\"1\" width=\"90%\" >");
				//display head of table
				out.println("<tr>");
				out.println("<td width=\"5\" >"+id+"</td>");
				for(String e:keys)
			    {
					if(e.equalsIgnoreCase("name")||e.equalsIgnoreCase("Adress"))
						out.println("<td width=\"17%\">"+e+"</td>");
					else if(e.equalsIgnoreCase("PNC"))
							out.println("<td width=\"12%\">"+e+"</td>");
					else out.println("<td>"+e+"</td>");
			    }
				out.println("<td width=\"5%\" >Operation</td>");
				out.println("</tr>");
				//display rest of table
				for (int i = 0; i < numRows; i++) 
				{
					int crtColNo = 0;
					out.println("<tr>");
					out.println("<td> "+table.get(id).get(i)+"<input type=\"hidden\" name=\""+id+i+"\" value=\""+table.get(id).get(i)+"\"></td>");
					
						for(String e:keys)
				    	{	
							if(e.equalsIgnoreCase("admin"))
							
							{
								out.println("<td width=\"5%\"> ");
								boolean var;
								byte val;
								if(table.get(e).get(i).equals("1"))
								{
									var=true;
									val=1;
								}
								else {
									var=false;
									val=0;
								}
								%> 
							
										<select name="<%=e+i %>" >
						            		<option value="<%=val%>" label="<%=var%>"/>
				            				<option value="<%=(val+1)%2%>" label="<%=!var%>"/>
				        				</select> <%
				        		out.println("</td>");
							}
							else out.println("<td>  <input type=\"case\" name=\""+e+i+"\" value=\""+table.get(e).get(i)+"\"></td>");
								  
				    	}
						
				
					out.println("<td><input type=\"submit\" name=\"remove"+table.get(id).get(i)+"\" value=\"Delete\"></td>");
					out.println("</tr>");
				}
				//display insert row
				out.println("<tr>");
				out.println("<td> </td>");
				for(String e:keys)
				{	
					if(e.equalsIgnoreCase("admin"))
						
					{
						out.println("<td width=\"7%\"> ");
						
						%> 
								<select name="<%=e%>i" >
				            		
		            				<option value="<%=0%>" label="<%=false%>"/>
		        				</select> <%
		        		out.println("</td>");
		        		}
					else out.println("<td > <input type=\"case\" name=\""+e+"i\" placeholder=\"Type here new information\"></td>");
						  
				}
				out.println("<td> </td>");
				out.println("</tr>");
				out.println("</table>");
		
			}else out.println("NO RESULT FOUND");
		}else out.println("NO RESULT FOUND");
		
	%>
	<input type="submit" name="oApply" value="Apply">
	</form>
	<br>
	
	</fieldset>
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
<script>
	var paramOne ="${message}";
	if(paramOne!=null && paramOne!="" && paramOne.length!=0)
		alert("message: "+paramOne);
</script>
</body>
</html>
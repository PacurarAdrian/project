package model.source;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class UserTable extends Table {

	public UserTable(Connection connArg, String dbNameArg, String dbmsArg) {
	    super();
	    this.con = connArg;
	    this.dbName = dbNameArg;
	    this.dbms = dbmsArg;
	    this.tableName="user"; 
	  }
	 public void createTable() throws SQLException {
		    String createString =
		    		"CREATE TABLE `"+dbName+"`.`"+tableName+"` ("+
		    				" `ID` INT NOT NULL AUTO_INCREMENT,"+
    						 " `Name` VARCHAR(45) NOT NULL,"+
    						 " `PNC` BIGINT(10) NOT NULL,"+
    						 " `Adress` VARCHAR(45) NULL,"+
    						 " `Salary` DOUBLE NOT NULL,"+
    						 " `Hire_date` DATE NOT NULL,"+
    						 " `Admin` TINYINT(1) NOT NULL DEFAULT 0,"+
    						 " `Username` VARCHAR(45) NOT NULL,"+
    						 " `Password` VARCHAR(45) NOT NULL,"+
    						 " PRIMARY KEY (`ID`),"+
    						 " UNIQUE INDEX `NPC_UNIQUE` (`PNC` ASC),"+
    						 " UNIQUE INDEX `Username_UNIQUE` (`Username` ASC),"+
    						 " UNIQUE INDEX `Password_UNIQUE` (`Password` ASC))";
		    Statement stmt = null;
		    try {
		      stmt = con.createStatement();
		      stmt.executeUpdate(createString);
		      stmt.executeUpdate("insert into  " +tableName+
                      " values( 1,'Administrator','1111111111111', '',  '1000', '1900-01-01', '1', 'username', 'parola' )");
		    } catch (SQLException e) {
		      JDBCUtilities.printSQLException(e);
		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
		  }
	  public void populateTable() throws SQLException {
	    Statement stmt = null;
	    try {
	    	stmt = con.createStatement();
	        stmt.executeUpdate("insert into  " +tableName+
	                           " values( 2, 'Stefanescu Delavrancea','1483243342354', '',  '4613.45', '1984-12-23', '0', 'std_dev', 'deundemam' )");
	        stmt.executeUpdate("insert into " +tableName+
	                           " values( 3, 'Victor Babes','3346528342201', '',  '3628.84', '1965-09-17', '0', 'victor', 'VBabesh')");
	        stmt.executeUpdate("insert into  " +tableName+
	                           " values( 4, 'Razvan Ursut','2039849231025', '',  '5246.00', '1993-08-23', '0', 'razvan', 'uzzi94')");
	     
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }

  
	 
	  
	  public void deleteRow(String id) throws SQLException {
		  Statement stmt = null;
		  String query = "DELETE FROM "+tableName+" WHERE `ID`='"+id+"'";
		    try {
		    	 stmt = con.createStatement();
		        stmt.executeUpdate(query);


		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
	  }  
	 

	 



	  public static void main(String[] args) {
	    JDBCUtilities myJDBCUtilities;
	    Connection myConnection = null;

	   
	    
	    myJDBCUtilities = new JDBCUtilities();
	     
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      UserTable table =
	        new UserTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	      System.out.println("\nDropping table:");
	     table. dropTable();
	     
	      System.out.println("\nCreating table:");
	      table.createTable();
	      System.out.println("\nPopulating table:");
	     table.populateTable();
	      System.out.println("\nContents of table:");
	      
	    
	     System.out.println(
	    		 table.getTable());
	     System.out.println(JDBCUtilities.getTable(myConnection, table.getTableName()));
	     /*
	      System.out.println("\n Viewing a employee:");
	      System.out.println(
	    		  employeeTable.findPnc(myConnection,"148324334235"));
	     
	      System.out.println("\nDeleting a row:");
	      employeeTable.deleteRow("148324334235");
	      EmployeeTable.viewTable(myConnection);
	     
	     System.out.println("\nDeleting table:");
	     employeeTable.deleteAllRows();
	     EmployeeTable.viewTable(myConnection);
	      

	      System.out.println("\nPopulating table:");
	      employeeTable.populateTable();
	      EmployeeTable.viewTable(myConnection);
	    
	      
	      
	      System.out.println("\nInserting a new row:");
	      employeeTable.insertRow("Kona Diana", "328492218432",2843.23, "2006-07-23");
	      EmployeeTable.viewTable(myConnection);
	      
	      
	      System.out.println("\nUpdating table:");
	      employeeTable.updateTable( JDBCUtilities.getTable(myConnection,TABLE_NAME));
	      EmployeeTable.viewTable(myConnection);
	      
	   /*
	      System.out.println("\nUpdating table:");
	      HashMap<Integer,ArrayList<String>> newInfo = new HashMap<Integer,ArrayList<String>>();
	      ArrayList<String> client1 = new ArrayList<String>();
	      client1.add(0, "Ana Dimiu");
	      client1.add(1, "394852809869");
	      client1.add(2, "3415.27");
	      client1.add(3, "2002-03-05");
	      newInfo.put(1,client1);
	      ArrayList<String> stud2 = new ArrayList<String>();
	      stud2.add(0, "Gashpar Ion");
	      stud2.add(1, "348294384235");
	      stud2.add(2, "9009.12");
	      stud2.add(3, "2004-05-03");
	      
	      newInfo.put(2,stud2);
	    //  System.out.println(newStudentInfo);
	      employeeTable.updateTable(newInfo);
	     EmployeeTable.viewTable(myConnection);
	     /*
	*/
	     
	      

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } catch (Exception e) {
	        e.printStackTrace(System.err);
	    } finally {
	      JDBCUtilities.closeConnection(myConnection);
	    }
	  }
	
}

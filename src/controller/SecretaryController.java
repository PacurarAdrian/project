package controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.SecretarySession;
import model.StoreException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/secretary")
public class SecretaryController {
	 @RequestMapping(value = "/home", method = RequestMethod.GET)
	    public ModelAndView home(@ModelAttribute("SpringWeb")SecretarySession session, ModelMap model) {
		   
		 try {
				model.addAttribute("accountTable",session.printTableAccount());
				
		       } catch (Exception e) {
					
			      e.printStackTrace();
				  model.addAttribute("accountTable",null);
				  
			   }
		       try {
				model.addAttribute("terrainTable",session.printTableTerrain());
				} catch (Exception e) {
					
					e.printStackTrace();
					model.addAttribute("terrainTable",null);
				}
		       try {
				model.addAttribute("infoTable",session.printTableClient());
				} catch (Exception e) {
					
					e.printStackTrace();
					model.addAttribute("terrainTable",null);
				}
		       List<String> searchList = new ArrayList<String>();
		          searchList.add("Terrain");
		          searchList.add("Account");
		          searchList.add("Information");
		          model.addAttribute("searchlist", searchList);
		      
	    
	    	 
	    	 
	 	       
	 	       return new ModelAndView("secretaryHome", "command", session);
	     
	    }
	 	
	  
	 	
	    
		@RequestMapping(value="/searchDelete",params="search",method=RequestMethod.POST)
	    public ModelAndView search(@ModelAttribute("SpringWeb")SecretarySession session, ModelMap model) {
	  	   
	       String temp=session.getOpValue();
	       String opSearch=session.getOpSearch();
	       List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList);  
	       if(temp==""|| temp==null)
	       {
	    	   switch(opSearch)
	    	   {
	    	   default:
		    	   try{
					model.addAttribute("table", session.printTableClient());
		    	   }
					catch(Exception e){
						throw new StoreException("Search could not be displayed");
					}
		    	   break;
	    	   case "Account":
	    		   try{
					model.addAttribute("table", session.printTableAccount());
		    	   }
					catch(Exception e){
						throw new StoreException("Search could not be displayed");
					}
		    	   break;
	    	   case "Terrain":
	    		   try{
					model.addAttribute("table", session.printTableTerrain());
		    	   }
					catch(Exception e){
						throw new StoreException("Search could not be displayed");
					}
		    	   break;	   
		    	   
	    	   }
			    	
	       }else{
	    	   String regex=temp;
	    	   Pattern p = Pattern.compile("[0-9]{13}");
	  	    	Matcher m = p.matcher(regex);
	  	    	if (!m.matches())
	  	    	{
	  	    		try {
						model.addAttribute("table", session.searchClientName(temp));
					} catch (Exception e) {
						throw new StoreException("User search could not be displayed");
					}
	  	    		return new ModelAndView("secretaryInfoEdit", "command", session);
	  	    	}else{
	  	    		switch(opSearch)
	 	    	   {
	 	    	   default:
						try {
							model.addAttribute("table", session.searchClientPnc(temp));
						} catch (Exception e) {
							
							e.printStackTrace();
							throw new StoreException("Search could not be displayed");
						}
					break;
	 	    	    case "Account":
						try {
							model.addAttribute("table", session.searchAccountPnc(temp));
						} catch (Exception e) {
							
							e.printStackTrace();
							throw new StoreException("Search could not be displayed");
						}
					break;
	 	    	    case "Terrain":
						try {
							model.addAttribute("table", session.searchTerrainPnc(temp));
						} catch (Exception e) {
							
							e.printStackTrace();
							throw new StoreException("Search could not be displayed");
						}
					break;
	 	    	   }
	  	    	}
		    	   
	       }  
		   	    
	         	 
			switch(opSearch)
			{
				default: return new ModelAndView("secretaryInfoEdit", "command", session);
				
				case "Terrain":return new ModelAndView("secretaryTerrainEdit", "command", session); 
				case "Account":return new ModelAndView("secretaryAccountEdit", "command", session); 
				
			}
	    }
		 @RequestMapping(value = "/accountEdit", method = RequestMethod.GET)
		   public ModelAndView accountEdit(@ModelAttribute("SpringWeb")SecretarySession session, ModelMap model) {
			   List<String> searchList = new ArrayList<String>();
		          searchList.add("Terrain");
		          searchList.add("Account");
		          searchList.add("Information");
		          model.addAttribute("searchlist", searchList);  
			   try {
					model.addAttribute("table", session.printTableAccount());
				} catch (Exception e) {
					
					e.printStackTrace();
					throw new StoreException("Table could not be displayed");
				}
			  
			   
		      return  new ModelAndView("secretaryAccountEdit", "command",session);
		   }
	   @RequestMapping(value = "/infoEdit", method = RequestMethod.GET)
	   public ModelAndView infoEdit(@ModelAttribute("SpringWeb")SecretarySession session, ModelMap model) {
		   List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList);  
		   try {
				model.addAttribute("table", session.printTableClient());
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("Table could not be displayed");
			}
		   //session.setOpBook(new Book());
		   
	      return  new ModelAndView("secretaryInfoEdit", "command",session);
	   }
	   @RequestMapping(value = "/terrainEdit", method = RequestMethod.GET)
	   public ModelAndView terrainEdit(@ModelAttribute("SpringWeb")SecretarySession session, ModelMap model) {
		   List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList);  
		   try {
				model.addAttribute("table", session.printTableTerrain());
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("Table could not be displayed");
			}
		   //session.setOpBook(new Book());
		   
	      return  new ModelAndView("secretaryTerrainEdit", "command",session);
	   }
	  private HashMap<String,ArrayList<String>> getUpdatedTable(HttpServletRequest request,SecretarySession session,Set<String> keys)
	  {
		  HashMap<String,ArrayList<String>> table= new HashMap<String,ArrayList<String>>();
			
		  
		    int nrRows=0,i=0;
		    String id="ID";
		    while(request.getParameter(id+i)!=null)
		    	i++;
		    nrRows=i;
			for (i = 0; i < nrRows; i++) 
			{
				for(String e:keys)
		    	{
				
					ArrayList<String> temp=table.get(e);
					if(temp==null)
							temp=new ArrayList<String>();
					temp.add(request.getParameter(e.toString()+i));
					table.put(e, temp);
		    		
		    	}
			}
			return table;
	  }
	  private HashMap<String,String> getInsertedInfo(HttpServletRequest request,SecretarySession session,Set<String> keys)
	  {
		  
	      String id="ID";
	       HashMap<String,String> insertInfo=new HashMap<String,String>();
	       boolean ok=false;
	       for(String e:keys)
		   	{
					if(!e.equalsIgnoreCase(id))
		   		{
						if(request.getParameter(e.toString()+"i")!=null && request.getParameter(e.toString()+"i")!="")
						{
							insertInfo.put(e, request.getParameter(e.toString()+"i"));
							ok=true;
						}else insertInfo.put(e,"");
		   		}
					
		   	}
	       
	       if(ok)
	       {
	    	   return insertInfo;
	       }else return null;
	  }
	  private void delete(SecretarySession session,HttpServletRequest request,String table) throws Exception {
 	       
      	
		   	
		   Enumeration<String> enume=request.getParameterNames();
		   while(enume.hasMoreElements())
		  {
			   String param=enume.nextElement();
			   System.out.println(param);
			   if(param.startsWith("remove"))
			   {
				   int id=Integer.parseInt(param.substring(6));
				   	switch(table)
				   	{
				   	case "Terrain": session.deleteTerrain(id);
				   	break;
				   	case "Account": session.deleteAccount(id);
				   	break;
				   	case "Information": session.deleteClient(id);
				   	break;
				   	default: throw new StoreException("Deletion could not be performed");
				   	
				   	
				   	}
		   			
		   			System.out.println("intra");
			   		
			   }
		   
		  }
		   		
			
	    }
	  @RequestMapping(value = "/terrainApply", method = RequestMethod.GET)
	   public ModelAndView terrainApply(HttpServletRequest request,ModelMap model) {
		   SecretarySession session=new SecretarySession();
		   List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList);  
	          HttpSession ses = request.getSession(true);
	      	
	  		  session.setSecretaryPnc((String)ses.getAttribute("PNC"));
	  		
		   try{
			   
			   delete(session,request,"Terrain");
			   
			   HashMap<String,ArrayList<String>> table=getUpdatedTable(request, session,session.getTerrainHeader());
		      System.out.println(table);
		      
		      //handle insert
		     
		       HashMap<String,String> insertInfo=getInsertedInfo(request, session,session.getTerrainHeader());
		       if(insertInfo!=null)
		       {
		       	//session.insertUser(insertInfo);
		       	model.addAttribute("table",session.editTerrain(table,insertInfo));
		       }
		       else  model.addAttribute("table",session.updateTerrain(table));
		       
		       
		   } catch (StoreException e) {
				
				e.printStackTrace();
				throw e;
			
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("User table could not be displayed");
			}
		   
	      return  new ModelAndView("secretaryTerrainEdit", "command",session);
	   }
	   @RequestMapping(value = "/infoApply", method = RequestMethod.GET)
	   public ModelAndView infoApply(HttpServletRequest request,ModelMap model) {
		   SecretarySession session=new SecretarySession();
		   
		   List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList);  
	          HttpSession ses = request.getSession(true);
		      	
	  		  session.setSecretaryPnc((String)ses.getAttribute("PNC"));
		   try{
			   
			   delete(session,request,"Information");
			   
			   HashMap<String,ArrayList<String>> table=getUpdatedTable(request, session,session.getInformationHeader());
		      System.out.println(table);
		      
		      //handle insert
		     
		       HashMap<String,String> insertInfo=getInsertedInfo(request, session,session.getInformationHeader());
		       if(insertInfo!=null)
		       {
		       	//session.insertUser(insertInfo);
		       	model.addAttribute("table",session.editClient(table,insertInfo));
		       }
		       else  model.addAttribute("table",session.updateClient(table));
		       
		       
		   } catch (StoreException e) {
				
				e.printStackTrace();
				throw e;
			
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("User table could not be displayed");
			}
		   
	      return  new ModelAndView("secretaryInfoEdit", "command",session);
	   }
	   @RequestMapping(value = "/accountApply", method = RequestMethod.GET)
	   public ModelAndView accountApply(HttpServletRequest request,ModelMap model) {
		   SecretarySession session=new SecretarySession();
		   List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList); 
	          HttpSession ses = request.getSession(true);
		      	
	  		  session.setSecretaryPnc((String)ses.getAttribute("PNC"));
		   try{
			   
			   delete(session,request,"Account");
			   
			   HashMap<String,ArrayList<String>> table=getUpdatedTable(request, session,session.getAccountHeader());
		      System.out.println(table);
		      
		      //handle insert
		     
		       HashMap<String,String> insertInfo=getInsertedInfo(request, session,session.getAccountHeader());
		       if(insertInfo!=null)
		       {
		       	//session.insertUser(insertInfo);
		       	model.addAttribute("table",session.editAccount(table,insertInfo));
		       }
		       else  model.addAttribute("table",session.updateAccount(table));
		       
		       
		   } catch (StoreException e) {
				
				e.printStackTrace();
				throw e;
			
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("User table could not be displayed");
			}
		   
	      return  new ModelAndView("secretaryAccountEdit", "command",session);
	   }
}
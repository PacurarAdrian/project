package model.source;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class ClientAccountTable extends Table{

  public ClientAccountTable(Connection connArg, String dbNameArg, String dbmsArg) {
    super();
    this.con = connArg;
    this.dbName = dbNameArg;
    this.dbms = dbmsArg;
    this.tableName="client_account";
  }
  public Set<String> getTableHeader() {

		Set<String> header=new HashSet<String>();
		header.addAll(Arrays.asList("ID","Type","Amount","PNC","Creation_date","Username","Password"));
		return header;
	}
  public void createTable() throws SQLException {
	    String createString =
	    		"CREATE TABLE `"+dbName+"`.`"+tableName+"` ("+
	    				 "  `ID` INT NOT NULL AUTO_INCREMENT,"+
	    				  " `Type` VARCHAR(45) NOT NULL,"+
	    				  " `Amount` DECIMAL(10,2) NOT NULL,"+
	    				  " `Creation_date` DATE NOT NULL,"+
	    				  " `PNC` BIGINT(13) NOT NULL,"+
	    				  " `Username` VARCHAR(45) NOT NULL,"+
 						 " `Password` VARCHAR(45) NOT NULL,"+
	    				  " PRIMARY KEY (`ID`),"+
	    				  " UNIQUE INDEX `id_UNIQUE` (`id` ASC),"+
	    				  " UNIQUE INDEX `NPC_UNIQUE` (`PNC` ASC),"+
	    				  " UNIQUE INDEX `Username_UNIQUE` (`Username` ASC),"+
    						" UNIQUE INDEX `Password_UNIQUE` (`Password` ASC));";
	    Statement stmt = null;
	    try {
	      stmt = con.createStatement();
	      stmt.executeUpdate(createString);
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }
  public void populateTable() throws SQLException {
    Statement stmt = null;
    try {
      stmt = con.createStatement();
      stmt.executeUpdate("insert into  " +tableName+
                         " values( 1, 'normal', '4264.33', '2001-03-23', '3482943842353', 'user1', 'pass1')");
      stmt.executeUpdate("insert into " +tableName+
                         " values( 2, 'normal', '6597.64', '2011-09-12', '3948528345204', 'user2', 'pass2')");
      stmt.executeUpdate("insert into  " +tableName+
                         " values( 3, 'normal', '9308.23', '2013-05-21', '2039389231022', 'user3', 'pass3')");
     
    } catch (SQLException e) {
      JDBCUtilities.printSQLException(e);
    } finally {
      if (stmt != null) { stmt.close(); }
    }
  }


  public void insertRow(String type, double amount, String date,String pnc) throws SQLException {
    Statement stmt = null;
    try {
      stmt =
          con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
      ResultSet uprs = stmt.executeQuery("SELECT * FROM "+tableName);

      uprs.moveToInsertRow();

      
     // uprs.updateInt("idstudent", studentID);
      uprs.updateString("type", type);
      uprs.updateString("PNC", pnc);
      uprs.updateDouble("amount", amount);
      uprs.updateString("creation_date", date);

      uprs.insertRow();
      uprs.beforeFirst();

    
    } finally {
      if (stmt != null) { stmt.close(); }
    }
  }
  
  
  public void viewTable() throws SQLException {
	  Statement stmt = null;
	  String query = "select ID,type,amount,creation_date,PNC from "+tableName;
	    
	    try {
	      stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery(query);

	      while (rs.next()) {
	        String id = rs.getString("ID");
	        String type = rs.getString("type");
	        String  amount = rs.getString("amount");
	        String  date = rs.getString("creation_date");
	        String pnc = rs.getString("PNC");
	        System.out.println(id + ", " + type + ", "+ amount + ", " +date+ ", " + pnc);
	      }

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }
  

  
  public void deleteRow(String id) throws SQLException {
	  Statement stmt = null;
	  String query = "DELETE FROM "+tableName+" WHERE `ID`='"+id+"'";
	    try {
	    	 stmt = con.createStatement();
	        stmt.executeUpdate(query);

	   
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
  }  

  public static void main(String[] args) {
    JDBCUtilities myJDBCUtilities;
    Connection myConnection = null;

   
    
    myJDBCUtilities = new JDBCUtilities();
     
    try {
      myConnection = myJDBCUtilities.getConnection();

      ClientAccountTable myClientAccount =
        new ClientAccountTable(myConnection, myJDBCUtilities.dbName,
                         myJDBCUtilities.dbms);
     System.out.println("\nDropping table:");
     myClientAccount. dropTable();
     
     System.out.println("\nCreating table:");
     myClientAccount.createTable();
     System.out.println("\nPopulating table:");
     myClientAccount.populateTable();
      System.out.println("\nContents of table:");
      myClientAccount.viewTable();
    
     System.out.println(myClientAccount.getTable());
     /*
      System.out.println("\n Viewing a client:");
      System.out.println(
      		 myClientAccount.findPnc("394852834520"));
     
      System.out.println("\nDeleting a row:");
      myClientAccount.deleteRow("394852834520");
      myClientAccount.viewTable();
     
     System.out.println("\nDeleting table:");
     myClientAccount.deleteAllRows();
     myClientAccount.viewTable();
      

      System.out.println("\nPopulating table:");
      myClientAccount.populateTable();
      myClientAccount.viewTable();
      
      
      
      System.out.println("\nInserting a new row:");
      myClientAccount.insertRow("regular",2843.23,"2003-03-12", "328492388432");
      myClientAccount.viewTable();
      
      System.out.println("\nUpdating table:");
      myClientAccount.updateTable(myClientAccount.getTable());
      myClientAccount.viewTable();
   /*
      System.out.println("\nUpdating table:");
      HashMap<Integer,ArrayList<String>> newInfo = new HashMap<Integer,ArrayList<String>>();
      ArrayList<String> client1 = new ArrayList<String>();
      client1.add(0, "saving");
      client1.add(1, "3415.27");
      client1.add(2, "2004-05-27");
      client1.add(3, "348294384235");
      newInfo.put(1,client1);
      ArrayList<String> stud2 = new ArrayList<String>();
      stud2.add(0, "spending");
      stud2.add(1, "9009.12");
      stud2.add(2, "2009-09-12");
      stud2.add(3, "394852834520");
      
      newInfo.put(2,stud2);
    //  System.out.println(newStudentInfo);
      myClientAccount.updateTable(newInfo);
      ClientAccountTable.viewTable(myConnection);
     /*
*/
     
      

    } catch (SQLException e) {
      JDBCUtilities.printSQLException(e);
    } catch (Exception e) {
        e.printStackTrace(System.err);
    } finally {
      JDBCUtilities.closeConnection(myConnection);
    }
  }
}

package model;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;


import javax.servlet.http.HttpServletResponse;

public class PDFReport implements Report {

	@Override
	public HttpServletResponse download(HttpServletResponse response,
			HashMap<String,ArrayList<String>> listBooks) throws IOException {
		//final ServletContext servletContext = request.getSession().getServletContext();
	      // final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
	      // final String temperotyFilePath = tempDirectory.getAbsolutePath();
	    
	       String fileName = "OperationHistory.pdf";
	       response.setContentType("application/pdf");
	       response.setHeader("Content-disposition", "attachment; filename="+ fileName);
	    
	       try {
	    
	           CreatePDF.createPDF(fileName,listBooks);
	           ByteArrayOutputStream baos = new ByteArrayOutputStream();
	           baos = convertPDFToByteArrayOutputStream(fileName);
	           OutputStream os = response.getOutputStream();
	           baos.writeTo(os);
	           os.flush();
	       } catch (IOException e1) {
	           e1.printStackTrace();
	           throw new StoreException("PDF download generation failed!");
	       }
		return response;
	}
	private ByteArrayOutputStream convertPDFToByteArrayOutputStream(String fileName) {
	    
		   InputStream inputStream = null;
		   ByteArrayOutputStream baos = new ByteArrayOutputStream();
		   try {
		    
		   inputStream = new FileInputStream(fileName);
		   byte[] buffer = new byte[1024];
		   baos = new ByteArrayOutputStream();
		    
		   int bytesRead;
		   while ((bytesRead = inputStream.read(buffer)) != -1) {
		   baos.write(buffer, 0, bytesRead);
		   }
		    
		   } catch (FileNotFoundException e) {
		   e.printStackTrace();
		   } catch (IOException e) {
		   e.printStackTrace();
		   } finally {
		   if (inputStream != null) {
		   try {
		   inputStream.close();
		   } catch (IOException e) {
		   e.printStackTrace();
		   }
		   }
		   }
		   return baos;
	   }
}

package model;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
 
import java.util.HashMap;
import java.util.Set;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class CreatePDF {
	
	 
	private static Font TIME_ROMAN = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
	private static Font TIME_ROMAN_SMALL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	 
	/**
	* @param args
	*/
	public static Document createPDF(String file,HashMap<String,ArrayList<String>> listBooks) {
	 
	Document document = null;
	 
	try {
	document = new Document();
	PdfWriter.getInstance(document, new FileOutputStream(file));
	document.open();
	 
	addMetaData(document);
	 
	addTitlePage(document);
	 
	createTable(document,listBooks);
	 
	document.close();
	 
	} catch (FileNotFoundException e) {
	 
	e.printStackTrace();
	} catch (DocumentException e) {
	e.printStackTrace();
	}
	return document;
	 
	}
	 
	private static void addMetaData(Document document) {
	document.addTitle("Generate user operation history PDF report");
	document.addSubject("User operation history report");
	document.addAuthor("administrator");
	document.addCreator("administrator");
	}
	 
	private static void addTitlePage(Document document)
	throws DocumentException {
	 
	Paragraph preface = new Paragraph();
	creteEmptyLine(preface, 1);
	preface.add(new Paragraph("Books out of stock PDF Report", TIME_ROMAN));
	 
	creteEmptyLine(preface, 1);
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	preface.add(new Paragraph("Report created on "
	+ simpleDateFormat.format(new Date()), TIME_ROMAN_SMALL));
	document.add(preface);
	 
	}
	 
	private static void creteEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
	 
	private static void createTable(Document document,HashMap<String,ArrayList<String>> listBooks) throws DocumentException {
		Paragraph paragraph = new Paragraph();
		creteEmptyLine(paragraph, 2);
		document.add(paragraph);
		
		 
		PdfPCell c1;
		Set<String> keys=listBooks.keySet();
		PdfPTable table = new PdfPTable(keys.size());
		for(String e:keys)
		{
			c1= new PdfPCell(new Phrase(e));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
		}
		int n=listBooks.get("ID").size();
		 
		
		table.setHeaderRows(1);
		 
		for (int i=0;i<n;i++) {
			table.setWidthPercentage(100);
			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			for(String e:keys)
			{
				table.addCell(listBooks.get(e).get(i));
				
			}
			
			
		}
		 
		document.add(table);
	}
	 
	

}

package testing;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import model.AdminSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import model.source.JDBCUtilities;

public class AdminSessionTest {
	AdminSession session;
	@Before
	public void setUp() throws Exception {
	    Connection myConnection = null;
	    JDBCUtilities myJDBCUtilities=new JDBCUtilities();
	    myConnection = myJDBCUtilities.getConnection();
	    JDBCUtilities.createDatabase(myConnection, "TerrainManagerTest", "mysql");
	    myConnection = myJDBCUtilities.getConnection();
	    JDBCUtilities.initializeTables(myConnection, "TerrainManagerTest","mysql");
	   session=new AdminSession();
	   
	}

	@After
	public void tearDown() throws Exception {
		JDBCUtilities myJDBCUtilities=new JDBCUtilities();
	    Connection myConnection = myJDBCUtilities.getConnection();
		JDBCUtilities.dropTables(myConnection, "TerrainManagerTest", "mysql");
	}

	@Test
	public void testSearchUser() throws Exception {
		//fail("Not yet implemented");
		ArrayList<String> emp=session.printTableUsers().get("Name");
		for(String e:emp)
			try {
				 HashMap<String,ArrayList<String>> table=session.searchUserName(e);
				 assertNotNull(table);
				 assertEquals(e,table.get("Name").get(0));
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e1) {
				 fail("exception occured");
				e1.printStackTrace();
			}
		try {
			 HashMap<String,ArrayList<String>> table=session.searchUserName("5123");
		System.out.println("\n"+table);
			assertNull(table.get("Name"));
			
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e1) {
			 fail("exception occured");
			e1.printStackTrace();
		}
		try {
			 HashMap<String,ArrayList<String>> table=session.searchUserName("");
			System.out.println("\n"+table);
			assertNotNull(table.get("Name"));
			 
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e1) {
			 fail("exception occured");
			e1.printStackTrace();
		}
	}
	@Test
	public void testSearchUserNpc() throws Exception {
		//fail("Not yet implemented");
		ArrayList<String> emp=session.printTableUsers().get("PNC");
		for(String e:emp)
			try {
				 HashMap<String,ArrayList<String>> table=session.searchUserNPC(e);
				 assertNotNull(table);
				 assertEquals(e,table.get("PNC").get(0));
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e1) {
				 fail("exception occured");
				e1.printStackTrace();
			}
		try {
			 HashMap<String,ArrayList<String>> table=session.searchUserNPC("Razvan Ursut");
		System.out.println("\n"+table);
			assertNull(table);
			
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e1) {
			 fail("exception occured");
			e1.printStackTrace();
		}
		try {
			 HashMap<String,ArrayList<String>> table=session.searchUserName("");
			System.out.println("\n"+table);
			assertNotNull(table.get("PNC"));
			 
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e1) {
			 fail("exception occured");
			e1.printStackTrace();
		}
	}
	@Test 
	public void testPrintTableUsers() throws Exception
	{
		HashMap<String,ArrayList<String>> newInfo = session.printTableUsers();
		System.out.println(newInfo);
		ArrayList<String> names = newInfo.get("Name");
		ArrayList<String> pass = newInfo.get("Password");
		ArrayList<String> pnc = newInfo.get("PNC");
		ArrayList<String> dates= newInfo.get("Hire_date");
	     
		ArrayList<String> salaries= newInfo.get("Salary");
		
		
		assertTrue(names.contains("Stefanescu Delavrancea")&&
				names.contains("Victor Babes")&&
				names.contains("Razvan Ursut")&&
				names.contains("Administrator")&&
				names.size()==4
				);
		assertTrue(pass.containsAll(Arrays.asList("parola","deundemam","uzzi94","VBabesh"))&&pass.size()==4
				);
		assertTrue(pnc.containsAll(Arrays.asList("1111111111111", "1483243342354", "3346528342201", "2039849231025"))&&pnc.size()==4
				);
		assertTrue(dates.containsAll(Arrays.asList("1900-01-01","1984-12-23","1993-08-23","1965-09-17"))&&dates.size()==4
				);
		assertTrue(salaries.containsAll(Arrays.asList("1000","4613.45","5246","3628.84"))&&salaries.size()==4
				);
		
	}
	@Test
	public void testUpdateUser() throws Exception
	{
		 System.out.println("\nUpdating table:");
	      HashMap<String,ArrayList<String>> newInfo = session.printTableUsers();
	      ArrayList<String> names = newInfo.get("Name");
	      names.set(0, "Ana Dimiu");
	      names.set(1, "Gashpar Ion");
	     // newInfo.put("name",names);
	      ArrayList<String> pass = newInfo.get("Password");
	      pass.add(0, "Trgues235");
	      pass.add(1, "Tiisraudimisr4");
	   //   newInfo.put("adress",adr);
	      ArrayList<String> isn = newInfo.get("PNC");
	      isn.add(0, "3415271234324");
	      isn.add(1, "9009123634334");
	     // newInfo.put("ICN",isn);
	      System.out.println(newInfo);
	      session.updateUser(newInfo);
	      
	      newInfo.clear();
	      newInfo = session.printTableUsers();
	      names = newInfo.get("Name");
	      assertTrue(names.get(0).equals("Ana Dimiu"));
	      assertTrue(names.get(1).equals("Gashpar Ion"));
	      
	      pass = newInfo.get("Password");
	      assertTrue(pass.get(0).equals("Trgues235"));
	      assertTrue(pass.get(1).equals("Tiisraudimisr4"));
	      
	      isn = newInfo.get("PNC");
	      assertTrue(isn.get(0).equals("3415271234324"));
	      assertTrue(isn.get(1).equals("9009123634334"));
	}
	@Test
	public void testDeleteUser() throws Exception
	{	
		String pnc="1483243342354";
		assertNotNull(session.searchUserNPC(pnc));
		session.deleteUser(pnc);
		assertNull(session.searchUserNPC(pnc));
	}
	@Test
	public void testInsertUser() throws Exception
	{
		 System.out.println("\nInserting table:");
	      HashMap<String,String> newInfo =  new HashMap<String,String>();
	      String name ;
	      name="Ana Dimiu";
	      newInfo.put("Name",name);
	      String user;
	      user="ana";
	      newInfo.put("Username",user);
	      String pass;
	      pass="Trgues235";
	      newInfo.put("Password",pass);
	      String pnc ;
	      pnc="3415271234324";
	      newInfo.put("PNC",pnc);
	      String date ;
	      date="2010-01-01";
	      newInfo.put("Hire_date",date);
	      String salary ;
	      salary="3454";
	      newInfo.put("Salary",salary);
	     
	      session.insertUser(newInfo);
	      
	      
	      newInfo.clear();
	      HashMap<String,ArrayList<String>> table = session.printTableUsers();
	      ArrayList<String> names = table.get("Name");
	      assertTrue(names.contains(name));
	      
	      ArrayList<String> passs = table.get("Password");
	      assertTrue(passs.contains(pass));
	      ArrayList<String> users = table.get("Username");
	      assertTrue(users.contains(user));
	      ArrayList<String> pncs = table.get("PNC");
	      assertTrue(pncs.contains(pnc));
	      ArrayList<String> dates = table.get("Hire_date");
	      assertTrue(dates.contains(date));
	}
}

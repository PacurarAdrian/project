<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=text], select{
   
 	
 	border-radius: 4px;
 	 
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}

div.vertical-line{
  width: 1px; /* Line width */
  background-color: silver; /* Line color */
  height: 100%; /* Override in-line if you want specific height. */
  display:inline-block;/*position:absolute; /*float: left; /* Causes the line to float to left of content. 
    You can instead use position:absolute or display:inline-block
    if this fits better with your design */
  border: 1px inset; /* This is default border style for <hr> tag */
  

}

</style>
<title>Hospital user</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Hospital</font></td><td width=10%></td></tr>

<tr >
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55% >
	<fieldset>
		<form:form method="GET" action="/Hospital/user/homeUser">
			<input type="submit" value="Home"/>
		</form:form>
		
		<form:form action="/BookStore/user/searchBook" method="POST">
			<form:input path="opValue" type="text" placeholder="search here"/>
			<form:select path="opSearch">
            		<form:option value="" label="--Please Select"/>
            		<form:options items="${searchlist}" />
        		</form:select>
			<input type="submit" name="search" value="Search"/>
			
		 </form:form>
		 		  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form:form method="GET" action="/TerrainManager/logout">Logged in:<%=session.getAttribute("name") %>  <input type="submit" value="Log out"/></form:form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<fieldset >
	<legend><h2>Book table</h2></legend>
	
	<br>
	
	<% if(request.getAttribute("table")!=null)
	{
			HashMap<String,ArrayList<String>> table=(HashMap<String,ArrayList<String>>)request.getAttribute("table");
			out.println("<table border=\"1\" width=\"90%\" >");
			int numRows =0;
			Set<String> keys=table.keySet();
			String id="ID";
			if(table.containsKey("ID"))
				numRows =table.get(id).size();
			else if(table.containsKey("id"))
			{
				id="id";
				numRows =table.get(id).size();
			}
			
			keys.remove(id);
			//display head of table
			out.println("<tr>");
			for(String e:keys)
		    {
				out.println("<td>"+e+"</td>");
		    }
			out.println("</tr>");
			//display rest of table
			for (int i = 0; i < numRows; i++) 
			{
				int crtColNo = 0;
				out.println("<tr>");
				for(String e:keys)
		    	{	
					out.println("<td> "+table.get(e).get(i)+"</td>");
					crtColNo++;
							    		  
		    	}
				out.println("</tr>");
				
			}
			out.println("</table>");
		}else out.println("NO BOOK INFORMATION");
	
	%>
	<br>
	
	<form:form method="GET" action="/BookStore/user/userSell">
		<form:input path="opValue" type="text" placeholder="Book ISBN"/>
		<input type="number" name="quantity" placeholder="quantity"/>
		<input type="submit" name="sell" value="Sell Book">
	</form:form>
	</fieldset>
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
</body>
</html>
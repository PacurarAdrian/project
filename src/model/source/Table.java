package model.source;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

public abstract class Table {
  protected String dbName;
  protected Connection con;
  protected String dbms;
  protected String tableName; 
  abstract public void createTable() throws SQLException ;
  
  public String getTableName()
  {
	  return new String(tableName);
  }
  abstract public void populateTable() throws SQLException ;


  public String updateTable(HashMap<String,ArrayList<String>> newInfo) throws SQLException {

	    PreparedStatement updateStatement = null;
	    Set<String> keys=newInfo.keySet();
	    
	    String updateString ="UPDATE "+tableName+" SET ";
	    int nrRows=0,nrCols=keys.size();
	    String id="ID";
	    for(String e:keys)
	    {
	    	if(e.equalsIgnoreCase(id))
	    	{
	    		nrRows=newInfo.get(e).size();
	    		id=e;
	    	}
	    	else updateString+=e+"=?, ";
	    	
	    }
	    updateString=updateString.substring(0, updateString.length()-2);
	    updateString+=" WHERE `"+id+"`=?";
	    

	    try {
	      con.setAutoCommit(false);
	      
	      updateStatement = con.prepareStatement(updateString);
	      
	      
	      for(int i=0;i<nrRows;i++)
	      {
	    	  int j=1;
	    	  for(String e:keys)
	    	  {
	    		  if(!e.equalsIgnoreCase(id))
	    		  {
	    			   updateStatement.setString(j, newInfo.get(e).get(i));
	    			   j++;
	    		  }
	    	  }
	    	  updateStatement.setString(nrCols, newInfo.get(id).get(i));
	    	  updateStatement.executeUpdate();
	    	  con.commit();
	      }
	      return null;
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	      
	      if (con != null) {
	        try {
	          System.err.print("Transaction is being rolled back");
	          con.rollback();
	        } catch (SQLException excep) {
	          JDBCUtilities.printSQLException(excep);
	          throw (SQLException)e.fillInStackTrace();
	        }
	      }
	      return e.getMessage();
	    } finally {
	      if (updateStatement != null) { updateStatement.close(); }
	     
	      con.setAutoCommit(true);
	    }
	  }
 


  public String insertRow(HashMap<String,String> info) throws SQLException {
	Statement stmt = null;
	PreparedStatement updateStatement = null;
    Set<String> keys=info.keySet();
    String updateString ="INSERT INTO "+tableName+" ( ";
    String updateString2 =") VALUES ( ";
    String id="ID";
    for(String e:keys)
    {
    	if(e.equalsIgnoreCase(id))
    	{
    		id=e;
    	}
    	else {
    		updateString+=e+", ";
    		updateString2+="?, ";
    	}
    		
    }
 // "INSERT INTO "+TABLE_NAME+" (`name`, `PNC`, `salary`, `hire_date`) VALUES ('dfa', '3143252346323', '3215', '2000-01-01');
    updateString=updateString.substring(0, updateString.length()-2)+updateString2.substring(0, updateString2.length()-2)+");";
    //System.out.println(updateString);
	try {
		con.setAutoCommit(false);
	      
	     updateStatement = con.prepareStatement(updateString);
	     int j=1;
	     for(String e:keys)
	   	  {
	   		  if(!e.equalsIgnoreCase(id))
	   		  {
	   			   updateStatement.setString(j, info.get(e));
	   			   j++;
	   		  }
	   	  }
	   	  //updateStudent.setString(nrCols, info.get(id));
	   	  updateStatement.executeUpdate();
	   	  con.commit();
	   	  return null;
	  } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	      
	      if (con != null) {
	        try {
	          System.err.print("Transaction is being rolled back");
	          con.rollback();
	        } catch (SQLException excep) {
	          JDBCUtilities.printSQLException(excep);
	          throw (SQLException)e.fillInStackTrace();
	        }
	      }
	      return e.getMessage();
	} finally {
	if (stmt != null) { stmt.close(); }
	}
	}

  
  
  public HashMap<String,ArrayList<String>> getTable() throws SQLException {
	  Statement stmt = null;
	  String query = "select * from "+tableName ;
	  HashMap<String,ArrayList<String>> info=new HashMap<String,ArrayList<String>>();
	 
	    try {
	      stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery(query);
	      ResultSetMetaData metaData=rs.getMetaData();
	      int nrCols=metaData.getColumnCount();
	      String[][] values=new String[nrCols+1][100];//max 100 row will be returned
	      int index=0;
	      while(rs.next() && index<100)
	      {
	        for(int i=1;i<=nrCols;i++)
	        {
	           values[i][index]= rs.getString(i);
	        }
	        index++;
	      }
	      for(int i=1;i<=nrCols;i++)
	      {
	    	  ArrayList<String> temp=new ArrayList<String>();
	    	  for(int j=0;j<index;j++)
		      {
		         temp.add(values[i][j]);
		      }
		        
	      	  info.put(metaData.getColumnLabel(i), temp);
	      	 // temp.clear();
	      }
	        return info;
	     
	    }catch(MySQLSyntaxErrorException e){
	     	e.printStackTrace();
	     	this.createTable();
	     	throw e;
	
	    } finally {
	      if (stmt != null) { stmt.close(); }
	     
	    }
	    
	  }
  
  public HashMap<String,ArrayList<String>> findPnc(String pnc) throws SQLException {
	  Statement stmt = null;
	  String query = "select * from "+tableName+" where pnc='"+pnc+"'";
	  HashMap<String,ArrayList<String>> clientInfo=new HashMap<String,ArrayList<String>>();
	    try {
	      stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery(query);
	      ResultSetMetaData metaData=rs.getMetaData();
	      int nrCols=metaData.getColumnCount();
	     if( rs.next())
	     {
	       
	        for(int i=1;i<=nrCols;i++)
	        {
	        	ArrayList<String> temp=new ArrayList<String>();
	        	temp.add(rs.getString(i));
	        	
	        	 clientInfo.put(metaData.getColumnLabel(i), temp);
	        }
	       // System.out.println(studentName + ", " + studentID + ", "+ birthDate + ", " + adress);
	       
	        return clientInfo;
	     }
	     else return null;

	    }catch(MySQLSyntaxErrorException e){
	     	e.printStackTrace();
	     	this.createTable();
	     	throw e;
	
	    
	    } finally {
	      if (stmt != null) { stmt.close(); }
	     
	    }
	    
	  }
  public void deleteRow(int pnc) throws SQLException {
	  Statement stmt = null;
	  String query = "DELETE FROM "+tableName+" WHERE `ID`='"+pnc+"'";
	    try {
	    	 stmt = con.createStatement();
	        stmt.executeUpdate(query);

	    }catch(MySQLSyntaxErrorException e){
	     	e.printStackTrace();
	     	this.createTable();
	     	throw e;
	
	    
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
  } 
  public void deleteRow(String pnc) throws SQLException {
	  Statement stmt = null;
	  String query = "DELETE FROM "+tableName+" WHERE `PNC`='"+pnc+"'";
	    try {
	    	 stmt = con.createStatement();
	        stmt.executeUpdate(query);

	    }catch(MySQLSyntaxErrorException e){
	     	e.printStackTrace();
	     	this.createTable();
	     	throw e;
	
	    
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
  }  
  public void deleteAllRows() throws SQLException {
	  Statement stmt = null;
	  String query = "DELETE FROM "+tableName;
	    try {
	    	 stmt = con.createStatement();
	        stmt.executeUpdate(query);

	    }catch(MySQLSyntaxErrorException e){
	     	e.printStackTrace();
	     	this.createTable();
	     	throw e;
	
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
  }  
 
  
  public Set<String> getNames() throws SQLException {
    HashSet<String> names = new HashSet<String>();
    Statement stmt = null;
    String query = "select name from "+tableName;
    try {
      stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(query);
      while (rs.next()) {
        names.add(rs.getString(1));
      }

    } finally {
      if (stmt != null) { stmt.close(); }
    }
    return names;
    
  }
  public Set<String> getPnc() throws SQLException {
    HashSet<String> names = new HashSet<String>();
    Statement stmt = null;
    String query = "select PNC from "+tableName;
    try {
      stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(query);
      while (rs.next()) {
        names.add(rs.getString(1));
      }

    } finally {
      if (stmt != null) { stmt.close(); }
    }
    return names;
    
  }
  public Set<Integer> getKeys() throws SQLException {
	    HashSet<Integer> keys = new HashSet<Integer>();
	    Statement stmt = null;
	    String query = "select id from "+tableName;
	    try {
	      stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery(query);
	      while (rs.next()) {
	        keys.add(rs.getInt(1));
	      }

	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	    return keys;
	    
	  }
  public void dropTable() throws SQLException {
	    Statement stmt = null;
	    try {
	      stmt = con.createStatement();
	      if (this.dbms.equals("mysql")) {
	        stmt.executeUpdate("DROP TABLE IF EXISTS "+tableName);
	      } else if (this.dbms.equals("derby")) {
	        stmt.executeUpdate("DROP TABLE "+tableName);
	      }
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }
}

package springSecurity.controller;

import java.util.ArrayList;
import java.util.List;

import model.AdminSession;
import model.SecretarySession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 

import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.ui.ModelMap;

@Controller

public class HelloController{
 
	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        model.addAttribute("greeting", "Hi, Welcome to mysite. ");
                   
        return "welcome";
    }
 
    @RequestMapping(value = "/admin", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView adminPage(ModelMap model,HttpServletRequest request) {
        //model.addAttribute("user", getPrincipal());
       // return "admin";
    	AdminSession session=new AdminSession();
    	HttpSession sess = request.getSession(true);
    	sess.setAttribute("name", "Administrator");
      //  model.addAttribute("username", getPrincipal());   
        
	      try {
			model.addAttribute("userTable", session.printTableUsers());
			
	       } catch (Exception e) {
				
		      e.printStackTrace();
		      model.addAttribute("userTable", null);
			 
		   }
	     
	      List<String> searchList = new ArrayList<String>();
	         searchList.add("Doctor");
	         searchList.add("Secretary");
	         model.addAttribute("searchlist", searchList);
	       return new ModelAndView("adminHome", "command", session);
    }
    @RequestMapping(value = "/secretary", method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView secretaryPage(ModelMap model,HttpServletRequest request) {
        //model.addAttribute("user", getPrincipal());
        //return "dba";
    	
    	AdminSession adminSession=new AdminSession();
    	
    	SecretarySession secrSession=new SecretarySession();
    	HttpSession session = request.getSession(true);
    	try {
    		
			session.setAttribute("name", adminSession.searchUsername(getPrincipal()).get("Name").get(0));
			session.setAttribute("PNC",adminSession.searchUsername(getPrincipal()).get("PNC").get(0));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	// model.addAttribute("name", getPrincipal());   
	       
	       try {
			model.addAttribute("accountTable",secrSession.printTableAccount());
			
	       } catch (Exception e) {
				
		      e.printStackTrace();
			  model.addAttribute("accountTable",null);
			  
		   }
	       try {
			model.addAttribute("terrainTable",secrSession.printTableTerrain());
			} catch (Exception e) {
				
				e.printStackTrace();
				model.addAttribute("terrainTable",null);
			}
	       try {
			model.addAttribute("infoTable",secrSession.printTableClient());
			} catch (Exception e) {
				
				e.printStackTrace();
				model.addAttribute("terrainTable",null);
			}
	       List<String> searchList = new ArrayList<String>();
	          searchList.add("Terrain");
	          searchList.add("Account");
	          searchList.add("Information");
	          model.addAttribute("searchlist", searchList);
	          model.addAttribute("message", "welcome home");
	       return new ModelAndView("secretaryHome", "command", secrSession);
    }
   
 
    @RequestMapping(value="/logout", method = RequestMethod.GET)
       public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
          Authentication auth = SecurityContextHolder.getContext().getAuthentication();
          if (auth != null){    
             new SecurityContextLogoutHandler().logout(request, response, auth);
          }
          return "welcome";
       }
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }
    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }
     
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

}
package model.domain;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.StoreException;
import model.source.HistoryTable;
import model.source.JDBCUtilities;

public class HistoryTableModule {
	private HistoryTable table;
	private JDBCUtilities myJDBCUtilities;
	private Connection myConnection = null;
	public  HistoryTableModule( )
	{
		
		myJDBCUtilities = new JDBCUtilities();
	
	}
	
	public HashMap<String,ArrayList<String>> update(String name,String pnc,String salary,String hire_date,String oldPnc) throws Exception
	{
		
	    try {
	    	
	    	  myConnection = myJDBCUtilities.getConnection();
	    	
	    	  table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	    	  HashMap<String,ArrayList<String>> temp=JDBCUtilities.getTable(myConnection, table.getTableName());
		      int index=-1;
		      index=temp.get("PNC").indexOf(oldPnc);
		      
	    	  HashMap<String,ArrayList<String>> newInfo = new HashMap<String,ArrayList<String>>();
    	      ArrayList<String> names = new ArrayList<String>();
    	      names.add(0, name);
    	      newInfo.put("name",names);
    	      ArrayList<String> pncs = new ArrayList<String>();
    	      pncs.add(0, pnc);
    	      newInfo.put("PNC",pncs);
    	      ArrayList<String> salarys = new ArrayList<String>();
    	      salarys.add(0, salary);
    	      newInfo.put("salary",salarys);
    	      ArrayList<String> dates = new ArrayList<String>();
    	      dates.add(0, hire_date);
    	      newInfo.put("hire_date",dates);
    	      ArrayList<String> id = new ArrayList<String>();
    	      id.add(0, temp.get("ID").get(index));/////////////////----------------!!!!!
    	      newInfo.put("ID",id);
    	      String rez;
		      if((rez=table.updateTable(newInfo))==null)
		    	  return JDBCUtilities.getTable(myConnection, table.getTableName());
		      else throw new Exception("Unable to execute update: "+rez);
		      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}

	public HashMap<String,ArrayList<String>> getTable() throws Exception
	{
		
	    try {
	    	
			 myConnection = myJDBCUtilities.getConnection();
			 table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
                     myJDBCUtilities.dbms);     		     
		     return table.getTable();
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	private void verifyInsert(HashMap<String,String> newInfo)
	{
		
	}
	private void verify(HashMap<String,ArrayList<String>> newInfo)
	{
		ArrayList<String> pncList=newInfo.get("PNC");
		ArrayList<String> salList=newInfo.get("Salary");
		ArrayList<String> hireList=newInfo.get("Hire_date");
		ArrayList<String> nameList=newInfo.get("Name");
		
		ArrayList<String> usernameList=newInfo.get("Username");
		
		
				
		Set<String> keys=new HashSet<String>(newInfo.keySet());
		keys.remove("Adress");
		for(String e:keys)
		{
			if(newInfo.get(e)==null||newInfo.get(e).contains(null)||newInfo.get(e).contains(""))
				throw new StoreException("Colum "+e+" cannot be empty");
		}
		
		Pattern p = Pattern.compile("[0-9]{13}");
		for(String e:pncList)
		{
	 	   Matcher m = p.matcher(e);
	 	   if (!m.matches())
	 		   throw new StoreException("invalid PNC: "+e);
					
		}
		p = Pattern.compile("[A-Z][a-z]+( [A-Z][a-z]+)+");
		for(String e:nameList)
		{
	 	   Matcher m = p.matcher(e);
	 	   if (!m.matches())
	 		   throw new StoreException("invalid Name: "+e);
				
		}
		
		p=Pattern.compile("[^\\p{Space}]+");
		for(String e:usernameList)
		{
	 	   Matcher m = p.matcher(e);
	 	   if (!m.matches())
	 		   throw new StoreException("invalid username: "+e);
					
		}
		double sal;
		for(String e:salList)
		{
			
			try{
				sal=Double.parseDouble(e);
			}catch(Exception exc)
			{
				throw new StoreException("invalid salary: "+e);
			}
			if(sal<=0)
		
				throw new StoreException("Salary cannot be negative or 0");
		}
		
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		for(String e:hireList)
			try {
				ft.parse(e);
			} catch (ParseException exc) {
				exc.printStackTrace();
				throw new StoreException("invalid hire date: "+e+"\nHire date format is yyyy-MM-dd");
				
			}
		
		
	}
	public HashMap<String,ArrayList<String>> update(HashMap<String,ArrayList<String>> newInfo) throws Exception
	{
		verify(newInfo);
	    try {
	    	
			 myConnection = myJDBCUtilities.getConnection();
			
			 table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
                     myJDBCUtilities.dbms);
			 
			 
			 if(table.updateTable(newInfo)==null)
			 {
				 return JDBCUtilities.getTable(myConnection, table.getTableName());
		    	 
			 }
		     else throw new StoreException("Unable to execute update");
		     
		     
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e;
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	public void insert(HashMap<String,String> newInfo) throws Exception
	{
		verifyInsert(newInfo);
	    try {
	    	
			 myConnection = myJDBCUtilities.getConnection();
			
			 table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
                     myJDBCUtilities.dbms);
			 
		     if(table.insertRow(newInfo)!=null)
		    	// return JDBCUtilities.getTable(myConnection, table.getTableName());
		        throw new Exception("Unable to execute insert");
		     

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	public HashMap<String, ArrayList<String>> edit(
			HashMap<String, ArrayList<String>> upInfo,
			HashMap<String, String> newInfo) throws Exception {
		verifyInsert(newInfo);
		verify(upInfo);
	    try {
	    	
			 myConnection = myJDBCUtilities.getConnection();
			
			 table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
                     myJDBCUtilities.dbms);
			 
			 if(table.insertRow(newInfo)!=null)
			    	// return JDBCUtilities.getTable(myConnection, table.getTableName());
			        throw new Exception("Unable to execute insert");
			 if(table.updateTable(upInfo)==null)
			 {
				 return JDBCUtilities.getTable(myConnection, table.getTableName());
		    	 
			 }
		     else throw new StoreException("Unable to execute update");
			 			 

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		
	}
	public HashMap<String,ArrayList<String>> delete(String pnc) throws Exception
	{
		
	    try {
	    	
			 myConnection = myJDBCUtilities.getConnection();
		
			 table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                     myJDBCUtilities.dbms);
		     if(pnc.length()!=13)
		    	 throw new Exception("Invalid NPC");
		     table.deleteRow(pnc);
		     
		     return JDBCUtilities.getTable(myConnection, table.getTableName());
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	public HashMap<String,ArrayList<String>> searchUserName(String name) throws Exception
	{
		
	    try {
	    	myConnection = myJDBCUtilities.getConnection();
	    	
    		table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
                         myJDBCUtilities.dbms);
      HashMap<String,ArrayList<String>> hashTable=	table.getTable();
	    	
	      HashMap<String,ArrayList<String>> temp=	new HashMap<String,ArrayList<String>>();
	   
	      ArrayList<String> names=hashTable.get("Username");
	     // System.out.println(hashTable);
	      for(String e:names)
	    	  if(e.toLowerCase().equals(name.toLowerCase()))
	    	{
	    		  int index=names.indexOf(e);
	    		  for(String key:hashTable.keySet())
	    		  {
	    			  ArrayList<String> tempList= temp.get(key);
	    			  if(tempList!=null)
	    				  tempList.add(hashTable.get(key).get(index));
	    			  else {
	    				  tempList=new ArrayList<String>();
	    				  tempList.add(hashTable.get(key).get(index));
	    			  }
	    			  temp.put(key, tempList);
	    		  }
	    	}
	    			  
	      
	      return temp;
	      

	    } catch (Exception e) {
	        throw (Exception)e;
	        
	    }
		    
		
	}
	public HashMap<String,ArrayList<String>> findName(String name) throws Exception
	{
		String pnc;
	    try {
	    	
	    		myConnection = myJDBCUtilities.getConnection();
	    	
	    		table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      HashMap<String,ArrayList<String>> temp=	JDBCUtilities.getTable(myConnection, table.getTableName());
	      int index=-1;
	    
	      if(temp.get("name").contains(name))
	    	 index=temp.get("name").indexOf(name);
	      pnc=temp.get("PNC").get(index);
	      return table.findPnc( pnc);
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	public HashMap<String,ArrayList<String>> findPnc(String pnc) throws Exception
	{
 
	    try {
	    	
	    		myConnection = myJDBCUtilities.getConnection();
	    	
	    		table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	   
	      return table.findPnc(pnc);
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	public Set<String> getNames() throws Exception
	{
 
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	   
	      return table.getNames();
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	public Set<String> getPnc() throws Exception
	{
 
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      table = new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	   
	      return table.getPnc();
	      

	    } catch (SQLException e) {
	        JDBCUtilities.printSQLException(e);
	        throw (SQLException)e.fillInStackTrace();
	        
	    } catch (Exception e) {
	        throw (Exception)e.fillInStackTrace();
	        
        } finally {
            JDBCUtilities.closeConnection(myConnection);
        }
		    
		
	}
	
	
	
	public static void main(String[] args) {
		
		HistoryTableModule module=new HistoryTableModule();
		try {
			System.out.println(module.getTable());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		

	}

}

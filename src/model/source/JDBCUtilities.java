package model.source;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.sql.SQLWarning;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

public class JDBCUtilities {

  public String dbms;
  public String jarFile;
  public String dbName; 
  public String userName;
  public String password;
  public String urlString;
  
  private String driver;
  private String serverName;
  private int portNumber;
//  private Properties prop;
  
  public JDBCUtilities()  {
	  super();
	  this.setProperties();
  }
  public static void dropTables(Connection con, String dbNameArg, String dbmsArg) throws SQLException {
	    TerrainTable myTerrainTable = new TerrainTable(con, dbNameArg, dbmsArg);
	    HistoryTable myHistoryTable = new HistoryTable(con, dbNameArg, dbmsArg);
	    UserTable myUserTable = new UserTable(con, dbNameArg, dbmsArg);
	    ClientAccountTable myClientAccountTable = new ClientAccountTable(con, dbNameArg, dbmsArg);
	    ClientInformationTable myInfoTable = new ClientInformationTable(con, dbNameArg, dbmsArg);
	    
	    
	    System.out.println("\nDropping exisiting  tables");
	    
	    myTerrainTable.dropTable();
	    myHistoryTable.dropTable();
	    myUserTable.dropTable();
	    myInfoTable.dropTable();
	    myClientAccountTable.dropTable();
	    
	    
	   
	   
	  } 
  public static void initializeTables(Connection con, String dbNameArg, String dbmsArg) throws SQLException {
	    TerrainTable myTerrainTable = new TerrainTable(con, dbNameArg, dbmsArg);
	    HistoryTable myHistoryTable = new HistoryTable(con, dbNameArg, dbmsArg);
	    UserTable myUserTable = new UserTable(con, dbNameArg, dbmsArg);
	    ClientAccountTable myClientAccountTable = new ClientAccountTable(con, dbNameArg, dbmsArg);
	    ClientInformationTable myInfoTable = new ClientInformationTable(con, dbNameArg, dbmsArg);
	   // System.out.println("\nDropping exisiting  tables");
	    
	   // myTerrainTable.dropTable();
	    
	    System.out.println("\nCreating and populating Terrain table...");    
	    myTerrainTable.createTable();
	    myTerrainTable.populateTable();
	    
	    System.out.println("\nCreating and populating User table...");    
	    myUserTable.createTable();
	    myUserTable.populateTable();
	    
	    System.out.println("\nCreating and populating Client account table...");    
	    myClientAccountTable.createTable();
	    myClientAccountTable.populateTable();
	    
	    System.out.println("\nCreating and populating information table...");    
	    myInfoTable.createTable();
	    myInfoTable.populateTable();

	    System.out.println("\nCreating and populating History table...");    
	    myHistoryTable.createTable();
	    myHistoryTable.populateTable();
	  } 

  public static void getWarningsFromResultSet(ResultSet rs) throws SQLException {
    JDBCUtilities.printWarnings(rs.getWarnings());
  }

  public static void getWarningsFromStatement(Statement stmt) throws SQLException {
    JDBCUtilities.printWarnings(stmt.getWarnings());
  }  

  public static void printWarnings(SQLWarning warning) throws SQLException {
    if (warning != null) {
      System.out.println("\n---Warning---\n");
      while (warning != null) {
        System.out.println("Message: " + warning.getMessage());
        System.out.println("SQLState: " + warning.getSQLState());
        System.out.print("Vendor error code: ");
        System.out.println(warning.getErrorCode());
        System.out.println("");
        warning = warning.getNextWarning();
      }
    }
  }

  public static boolean ignoreSQLException(String sqlState) {
    if (sqlState == null) {
      System.out.println("The SQL state is not defined!");
      return false;
    }
    // X0Y32: Jar file already exists in schema
    if (sqlState.equalsIgnoreCase("X0Y32"))
      return true;
    // 42Y55: Table already exists in schema
    if (sqlState.equalsIgnoreCase("42Y55"))
      return true;
    return false;
  }



  public static void printSQLException(SQLException ex) {
    for (Throwable e : ex) {
      if (e instanceof SQLException) {
        if (ignoreSQLException(((SQLException)e).getSQLState()) == false) {
          e.printStackTrace(System.err);
          System.err.println("SQLState: " + ((SQLException)e).getSQLState());
          System.err.println("Error Code: " + ((SQLException)e).getErrorCode());
          System.err.println("Message: " + e.getMessage());
          Throwable t = ex.getCause();
          while (t != null) {
            System.out.println("Cause: " + t);
            t = t.getCause();
          }
        }
      }
    }
  }

  public static void alternatePrintSQLException(SQLException ex) {
    while (ex != null) {
      System.err.println("SQLState: " + ex.getSQLState());
      System.err.println("Error Code: " + ex.getErrorCode());
      System.err.println("Message: " + ex.getMessage());
      Throwable t = ex.getCause();
      while (t != null) {
        System.out.println("Cause: " + t);
        t = t.getCause();
      }
      ex = ex.getNextException();
    }
  }

  private void setProperties()  {
  
    this.dbms = "mysql";
    this.jarFile = null;
    this.driver = null;
    this.dbName = "terrainManagerTest";
    this.userName = "root";
    this.password = "D3V1Lb0n3";
    this.serverName = "localhost";
    this.portNumber = 3306;

    System.out.println("Set the following properties:");
    System.out.println("dbms: " + dbms);
    System.out.println("driver: " + driver);
    System.out.println("dbName: " + dbName);
    System.out.println("userName: " + userName);
    System.out.println("serverName: " + serverName);
    System.out.println("portNumber: " + portNumber);

  }


  public Connection getConnection() throws SQLException,Exception {
    Connection conn = null;
    Properties connectionProps = new Properties();
    connectionProps.put("user", this.userName);
    connectionProps.put("password", this.password);
    
    String currentUrlString = null;

    if (this.dbms.equals("mysql")) {
      currentUrlString = "jdbc:" + this.dbms + "://" + this.serverName +
                                      ":" + this.portNumber + "/";
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      conn =
          DriverManager.getConnection(currentUrlString,
                                      connectionProps);
      
      this.urlString = currentUrlString + this.dbName;
     try{ conn.setCatalog(this.dbName);
     }catch(MySQLSyntaxErrorException e){
     	e.printStackTrace();
     	  JDBCUtilities.createDatabase(conn,this.dbName,this.dbms);
     	  conn.setCatalog(this.dbName);
          JDBCUtilities.initializeTables(conn,this.dbName,this.dbms);
     }
    } else if (this.dbms.equals("derby")) {
      this.urlString = "jdbc:" + this.dbms + ":" + this.dbName;
      
      conn =
          DriverManager.getConnection(this.urlString + 
                                      ";create=true", connectionProps);
      
    }
    System.out.println("Connected to database");
    return conn;
  }
  public static HashMap<String,ArrayList<String>> getTable(Connection con,String table) throws SQLException {
	  Statement stmt = null;
	  String query = "select * from "+table;
	  HashMap<String,ArrayList<String>> info=new HashMap<String,ArrayList<String>>();
	 
	    try {
	      stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery(query);
	      ResultSetMetaData metaData=rs.getMetaData();
	      int nrCols=metaData.getColumnCount();
	      String[][] values=new String[nrCols+1][100];//max 100 row will be returned
	      int index=0;
	      while(rs.next() && index<100)
	      {
	        for(int i=1;i<=nrCols;i++)
	        {
	           values[i][index]= rs.getString(i);
	        }
	        index++;
	      }
	      for(int i=1;i<=nrCols;i++)
	      {
	    	  ArrayList<String> temp=new ArrayList<String>();
	    	  for(int j=0;j<index;j++)
		      {
		         temp.add(values[i][j]);
		      }
		        
	      	  info.put(metaData.getColumnLabel(i), temp);
	      	 // temp.clear();
	      }
	        return info;
	     

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	      return null;
	    } finally {
	      if (stmt != null) { stmt.close(); }
	     
	    }
	    
	  }
  public static String insertRow(HashMap<String,String> info,Connection con,String table) throws SQLException {
		Statement stmt = null;
		PreparedStatement updateStudent = null;
	    Set<String> keys=info.keySet();
	    String updateString ="INSERT INTO "+table+" ( ";
	    String updateString2 =") VALUES ( ";
	    String id="ID";
	    for(String e:keys)
	    {
	    	if(e.equalsIgnoreCase(id))
	    	{
	    		id=e;
	    	}
	    	else {
	    		updateString+=e+", ";
	    		updateString2+="?, ";
	    	}
	    		
	    }
	 // "INSERT INTO "+TABLE_NAME+" (`name`, `PNC`, `salary`, `hire_date`) VALUES ('dfa', '3143252346323', '3215', '2000-01-01');
	    updateString=updateString.substring(0, updateString.length()-2)+updateString2.substring(0, updateString2.length()-2)+");";
	    //System.out.println(updateString);
		try {
			con.setAutoCommit(false);
		      
		     updateStudent = con.prepareStatement(updateString);
		     int j=1;
		     for(String e:keys)
		   	  {
		   		  if(!e.equalsIgnoreCase(id))
		   		  {
		   			   updateStudent.setString(j, info.get(e));
		   			   j++;
		   		  }
		   	  }
		   	  //updateStudent.setString(nrCols, info.get(id));
		   	  updateStudent.executeUpdate();
		   	  con.commit();
		   	  return null;
		  } catch (SQLException e) {
		      JDBCUtilities.printSQLException(e);
		      
		      if (con != null) {
		        try {
		          System.err.print("Transaction is being rolled back");
		          con.rollback();
		        } catch (SQLException excep) {
		          JDBCUtilities.printSQLException(excep);
		          throw (SQLException)e.fillInStackTrace();
		        }
		      }
		      return e.getMessage();
		} finally {
		if (stmt != null) { stmt.close(); }
		}
		}
  public static String updateTable(HashMap<String,ArrayList<String>> newInfo,Connection con,String table) throws SQLException {

	    PreparedStatement updateStatement = null;
	    Set<String> keys=newInfo.keySet();
	    
	    String updateString ="UPDATE "+table+" SET ";
	    int nrRows=0,nrCols=keys.size();
	    String id="ID";
	    for(String e:keys)
	    {
	    	if(e.equalsIgnoreCase(id))
	    	{
	    		nrRows=newInfo.get(e).size();
	    		id=e;
	    	}
	    	else updateString+=e+"=?, ";
	    	
	    }
	    updateString=updateString.substring(0, updateString.length()-2);
	    updateString+=" WHERE `"+id+"`=?";
	    

	    try {
	      con.setAutoCommit(false);
	      
	      updateStatement = con.prepareStatement(updateString);
	      
	      
	      for(int i=0;i<nrRows;i++)
	      {
	    	  int j=1;
	    	  for(String e:keys)
	    	  {
	    		  if(!e.equalsIgnoreCase(id))
	    		  {
	    			   updateStatement.setString(j, newInfo.get(e).get(i));
	    			   j++;
	    		  }
	    	  }
	    	  updateStatement.setString(nrCols, newInfo.get(id).get(i));
	    	  updateStatement.executeUpdate();
	    	  con.commit();
	      }
	      return null;
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	      
	      if (con != null) {
	        try {
	          System.err.print("Transaction is being rolled back");
	          con.rollback();
	        } catch (SQLException excep) {
	          JDBCUtilities.printSQLException(excep);
	          throw (SQLException)e.fillInStackTrace();
	        }
	      }
	      return e.getMessage();
	    } finally {
	      if (updateStatement != null) { updateStatement.close(); }
	     
	      con.setAutoCommit(true);
	    }
	  }
  public static void createDatabase(Connection connArg, String dbNameArg,String dbmsArg) {
	if (dbmsArg.equals("mysql")) {
		try {
				Statement s = connArg.createStatement();
				String newDatabaseString =
				"CREATE DATABASE IF NOT EXISTS " + dbNameArg;
				// String newDatabaseString = "CREATE DATABASE " + dbName;
				s.executeUpdate(newDatabaseString);
				
				System.out.println("Created database " + dbNameArg);
			} catch (SQLException e) {
				printSQLException(e);
			}
		}
	}

  

  public static void closeConnection(Connection connArg) {
    System.out.println("Releasing all open resources ...");
    try {
      if (connArg != null) {
        connArg.close();
        connArg = null;
      }
    } catch (SQLException sqle) {
      printSQLException(sqle);
    }
  }
  


  public static void main(String[] args) {
    JDBCUtilities myJDBCUtilities;
    Connection myConnection = null;
    
     
        
    myJDBCUtilities = new JDBCUtilities();
      
    

    try {
      myConnection = myJDBCUtilities.getConnection();
    
     

    } catch (SQLException e) {
      JDBCUtilities.printSQLException(e);
    } catch (Exception e) {
      e.printStackTrace(System.err);
    } finally {
      JDBCUtilities.closeConnection(myConnection);
    }

  }
}

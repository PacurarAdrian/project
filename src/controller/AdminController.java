package controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Set;






import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;






import javax.servlet.http.HttpServletResponse;

import model.AdminSession;
import model.Report;
import model.ReportFactory;
import model.StoreException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/admin")
public class AdminController {
	 @RequestMapping(value = "/home", method = RequestMethod.GET)
	    public ModelAndView home(@ModelAttribute("SpringWeb")AdminSession session, ModelMap model) {
		   
	    	 try {
	 			model.addAttribute("userTable", session.printTableUsers());
	 			
	 	       } catch (Exception e) {
	 				
	 		      e.printStackTrace();
	 		     model.addAttribute("table",null);
	 		   
	 		   }
	    	 
	    	 
	 	       model.addAttribute("message", "welcome home");
	 	       return new ModelAndView("adminHome", "command", session);
	     
	    }
	 	
	 @RequestMapping(value = "/downloadPDF")
	   public void downloadPDF( HttpServletResponse response,@ModelAttribute("SpringWeb")AdminSession session) {
	
	       try {
	    		 HashMap<String,ArrayList<String>> listBooks = session.getHistoryTable();
	  	       ReportFactory fact=new ReportFactory();
	  	       Report rep=fact.getReport("PDF");
			response=rep.download(response, listBooks);
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("PDF download generation failed!");
			}
	   
	    
	   }
	 	
	    
		@RequestMapping(value="/search",params="search",method=RequestMethod.POST)
	    public ModelAndView search(@ModelAttribute("SpringWeb")AdminSession session, ModelMap model) {
	  	   
	       String temp=session.getOpValue();
	      
	       if(temp==""|| temp==null)
	       {
	    	
	    	   try{
				model.addAttribute("table", session.printTableUsers());
	    	   }
				catch(Exception e){
					throw new StoreException("User search could not be displayed");
				}
			    	
	       }else{
	    	   String regex=temp;
	    	   Pattern p = Pattern.compile("[0-9]{13}");
	  	    	Matcher m = p.matcher(regex);
	  	    	if (!m.matches())
	  	    	{
	  	    		try {
						model.addAttribute("table", session.searchUserName(temp));
					} catch (Exception e) {
						throw new StoreException("User search could not be displayed");
					}
	  	    	}else{
	    	  
					try {
						model.addAttribute("table", session.searchUserNPC(temp));
					} catch (Exception e) {
						
						e.printStackTrace();
						throw new StoreException("Search could not be displayed");
					}
	  	    	}
		    	   
	       }  
		   	    
			    	 
			  
	       return new ModelAndView("adminEdit", "command", session);
	    }
	   
	   @RequestMapping(value = "/userEdit", method = RequestMethod.GET)
	   public ModelAndView adminEdit(@ModelAttribute("SpringWeb")AdminSession session, ModelMap model) {
		   
		   try {
				model.addAttribute("table", session.printTableUsers());
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("User table could not be displayed");
			}
		   //session.setOpBook(new Book());
		   
	      return  new ModelAndView("adminEdit", "command",session);
	   }
	  private HashMap<String,ArrayList<String>> getUpdatedTable(HttpServletRequest request,AdminSession session)
	  {
		  HashMap<String,ArrayList<String>> table= new HashMap<String,ArrayList<String>>();
			
		  Set<String> keys=session.getHeader(); 
		    int nrRows=0,i=0;
		    String id="ID";
		    while(request.getParameter(id+i)!=null)
		    	i++;
		    nrRows=i;
			for (i = 0; i < nrRows; i++) 
			{
				for(String e:keys)
		    	{
				
					ArrayList<String> temp=table.get(e);
					if(temp==null)
							temp=new ArrayList<String>();
					temp.add(request.getParameter(e.toString()+i));
					table.put(e, temp);
		    		
		    	}
			}
			return table;
	  }
	  private HashMap<String,String> getInsertedInfo(HttpServletRequest request,AdminSession session)
	  {
		  Set<String> keys=session.getHeader();
	      String id="ID";
	       HashMap<String,String> insertInfo=new HashMap<String,String>();
	       boolean ok=false;
	       for(String e:keys)
		   	{
					if(!e.equalsIgnoreCase(id))
		   		{
						if(request.getParameter(e.toString()+"i")!=null && request.getParameter(e.toString()+"i")!=""
								&& !e.equals("Admin"))
						{
							insertInfo.put(e, request.getParameter(e.toString()+"i"));
							ok=true;
						}else insertInfo.put(e,"");
		   		}
					
		   	}
	       insertInfo.put("Admin", request.getParameter("Admini"));
	       if(ok)
	       {
	    	   return insertInfo;
	       }else return null;
	  }
	  private void deleteUser(AdminSession session,HttpServletRequest request) throws Exception {
 	       
      	
		   	
		   Enumeration<String> enume=request.getParameterNames();
		   while(enume.hasMoreElements())
		  {
			   String param=enume.nextElement();
			   System.out.println(param);
			   if(param.startsWith("remove"))
			   {
				   int id=Integer.parseInt(param.substring(6));
				   	
		   			session.deleteUser(""+id);
		   			System.out.println("intra");
			   		
			   }
		   
		  }
		   		
			
	    }
	   @RequestMapping(value = "/userApply", method = RequestMethod.GET)
	   public ModelAndView adminApply(HttpServletRequest request,ModelMap model) {
		   AdminSession session=new AdminSession();
		   try{
			   
			   deleteUser(session,request);
			   
			   HashMap<String,ArrayList<String>> table=getUpdatedTable(request, session);
		      System.out.println(table);
		      
		      //handle insert
		     
		       HashMap<String,String> insertInfo=getInsertedInfo(request, session);
		       if(insertInfo!=null)
		       {
		       	//session.insertUser(insertInfo);
		       	model.addAttribute("table",session.editUser(table,insertInfo));
		       }
		       else  model.addAttribute("table",session.updateUser(table));
		       
		       
		   } catch (StoreException e) {
				
				e.printStackTrace();
				throw e;
			
			} catch (Exception e) {
				
				e.printStackTrace();
				throw new StoreException("User table could not be displayed");
			}
		   
	      return  new ModelAndView("adminEdit", "command",session);
	   }
}

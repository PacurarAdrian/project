<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=text], select{
   
 	
 	border-radius: 4px;
 	 
}
html {
    height: 100%;
    background-image: url(https://www.isc.ca/_layouts/IMAGES/ISCWebSite/global/background_image.jpg);
    background-repeat: repeat-x;
    background-color: Black;
    }
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}  box-sizing: border-box;
}
</style>
<title>Terrain manager welcome</title>
</head>

<body >
<table border="0" width=100%>
<tr ><td width=10%></td><td bgcolor=#D1D0D0 colspan=2><font size=10 face="Viner Hand ITC">Terrain manager</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		
		 		  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<fieldset >
	<legend><h2>Welcome</h2></legend>
	
	<h3>${greeting}</h3>
	You must login to access the other pages
	<br>
	<form:form method="GET" action="/TerrainManager/admin/">
			<input type="submit" value="Login as admin">
	</form:form>
	
	<form:form method="GET" action="/TerrainManager/secretary">
			<input type="submit" value="Login as user  ">
	</form:form>
	</fieldset>
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
</body>
</html>
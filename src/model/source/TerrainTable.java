package model.source;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

public class TerrainTable extends Table {

	public TerrainTable(Connection connArg, String dbNameArg, String dbmsArg) {
	    super();
	    this.con = connArg;
	    this.dbName = dbNameArg;
	    this.dbms = dbmsArg;
	    this.tableName="terrain"; 
	  }
	public Set<String> getTableHeader() {

		Set<String> header=new HashSet<String>();
		header.addAll(Arrays.asList("ID","Area","Type","OwnerPNC","Place"));
		return header;
	}
	  public void createTable() throws SQLException {
		    String createString =
		    		"CREATE TABLE `"+dbName+"`.`"+tableName+"` ("+
		    				 "`ID` INT NOT NULL AUTO_INCREMENT,"+
		    				  "`Type` VARCHAR(45) NOT NULL,"+
		    				  "`OwnerPNC`  VARCHAR(13) NULL ,"+
		    				  "`Area` DOUBLE NOT NULL,"+
		    				  "`Place` VARCHAR(45) NOT NULL,"+
		    				  "PRIMARY KEY (`ID`),"+
		    				  "UNIQUE INDEX `ID_UNIQUE` (`ID` ASC));";
		    Statement stmt = null;
		    try {
		      stmt = con.createStatement();
		      stmt.executeUpdate(createString);
		    } catch (SQLException e) {
		      JDBCUtilities.printSQLException(e);
		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
		  }
	  
	  public void populateTable() throws SQLException {
	    Statement stmt = null;
	    try {
	      stmt = con.createStatement();
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 1, 'teren_agricol','2039389231022', '345', 'Sat. Jucu de Sus(Com. Jucu) Jud. Cluj')");
	      stmt.executeUpdate("insert into " +tableName+
	                         " values( 2, 'teren_agricol','2039389231022', '413', 'Sat. Jucu de Sus(Com. Jucu) Jud. Cluj')");
	      
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }
	  
	  public HashMap<String,ArrayList<String>> findPnc(String pnc) throws SQLException {
		  Statement stmt = null;
		  String query = "select * from "+tableName+" where OwnerPNC='"+pnc+"'";
		  HashMap<String,ArrayList<String>> clientInfo=new HashMap<String,ArrayList<String>>();
		    try {
		      stmt = con.createStatement();
		      ResultSet rs = stmt.executeQuery(query);
		      ResultSetMetaData metaData=rs.getMetaData();
		      int nrCols=metaData.getColumnCount();
		     if( rs.next())
		     {
		       
		        for(int i=1;i<=nrCols;i++)
		        {
		        	ArrayList<String> temp=new ArrayList<String>();
		        	temp.add(rs.getString(i));
		        	
		        	 clientInfo.put(metaData.getColumnLabel(i), temp);
		        }
		       // System.out.println(studentName + ", " + studentID + ", "+ birthDate + ", " + adress);
		       
		        return clientInfo;
		     }
		     else return null;

		    }catch(MySQLSyntaxErrorException e){
		     	e.printStackTrace();
		     	this.createTable();
		     	throw e;
		
		    
		    } finally {
		      if (stmt != null) { stmt.close(); }
		     
		    }
		    
		  }
	  
	  public void deleteRow(String pnc) throws SQLException {
		  Statement stmt = null;
		  String query = "DELETE FROM "+tableName+" WHERE `PNC`='"+pnc+"'";
		    try {
		    	 stmt = con.createStatement();
		        stmt.executeUpdate(query);


		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
	  }  
	 

	 



	  public static void main(String[] args) {
	    JDBCUtilities myJDBCUtilities;
	    Connection myConnection = null;

	   
	    
	    myJDBCUtilities = new JDBCUtilities();
	     
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      SecretaryTable secretaryTable =
	        new SecretaryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	      System.out.println("\nDropping table:");
	      secretaryTable. dropTable();
	      
	      System.out.println("\nCreating table:");
	      secretaryTable.createTable();
	      System.out.println("\nPopulating table:");
	     secretaryTable.populateTable();
	      System.out.println("\nContents of table:");
	      
	    
	     System.out.println(
	    		 secretaryTable.getTable());
	     System.out.println(JDBCUtilities.getTable(myConnection, secretaryTable.getTableName()));
	     /*
	      System.out.println("\n Viewing a employee:");
	      System.out.println(
	    		  employeeTable.findPnc(myConnection,"148324334235"));
	     
	      System.out.println("\nDeleting a row:");
	      employeeTable.deleteRow("148324334235");
	      EmployeeTable.viewTable(myConnection);
	     
	     System.out.println("\nDeleting table:");
	     employeeTable.deleteAllRows();
	     EmployeeTable.viewTable(myConnection);
	      

	      System.out.println("\nPopulating table:");
	      employeeTable.populateTable();
	      EmployeeTable.viewTable(myConnection);
	    
	      
	      
	      System.out.println("\nInserting a new row:");
	      employeeTable.insertRow("Kona Diana", "328492218432",2843.23, "2006-07-23");
	      EmployeeTable.viewTable(myConnection);
	      
	      
	      System.out.println("\nUpdating table:");
	      employeeTable.updateTable( JDBCUtilities.getTable(myConnection,TABLE_NAME));
	      EmployeeTable.viewTable(myConnection);
	      
	   /*
	      System.out.println("\nUpdating table:");
	      HashMap<Integer,ArrayList<String>> newInfo = new HashMap<Integer,ArrayList<String>>();
	      ArrayList<String> client1 = new ArrayList<String>();
	      client1.add(0, "Ana Dimiu");
	      client1.add(1, "394852809869");
	      client1.add(2, "3415.27");
	      client1.add(3, "2002-03-05");
	      newInfo.put(1,client1);
	      ArrayList<String> stud2 = new ArrayList<String>();
	      stud2.add(0, "Gashpar Ion");
	      stud2.add(1, "348294384235");
	      stud2.add(2, "9009.12");
	      stud2.add(3, "2004-05-03");
	      
	      newInfo.put(2,stud2);
	    //  System.out.println(newStudentInfo);
	      employeeTable.updateTable(newInfo);
	     EmployeeTable.viewTable(myConnection);
	     /*
	*/
	     
	      

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } catch (Exception e) {
	        e.printStackTrace(System.err);
	    } finally {
	      JDBCUtilities.closeConnection(myConnection);
	    }
	  }
	}

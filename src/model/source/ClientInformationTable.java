package model.source;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class ClientInformationTable extends Table {

	public ClientInformationTable(Connection connArg, String dbNameArg, String dbmsArg) {
	    super();
	    this.con = connArg;
	    this.dbName = dbNameArg;
	    this.dbms = dbmsArg;
	    this.tableName="client_information"; 
	  }
	public Set<String> getTableHeader() {

		Set<String> header=new HashSet<String>();
		header.addAll(Arrays.asList("ID","Name","ICN","PNC","Adress"));
		return header;
	}
	 public void createTable() throws SQLException {
		    String createString =
		    		"CREATE TABLE `"+dbName+"`.`"+tableName+"` ("+
		    				  "`ID` INT(11) NOT NULL AUTO_INCREMENT,"+
		    				  "`Name` VARCHAR(45) NOT NULL,"+
		    				  "`ICN` BIGINT(6) NOT NULL,"+
		    				  "`PNC` BIGINT(13) NOT NULL,"+
		    				  "`Adress` VARCHAR(100) NOT NULL,"+
		    				  "PRIMARY KEY (`ID`),"+
		    				  "UNIQUE INDEX `ICN_UNIQUE` (`ICN` ASC),"+
		    				  "UNIQUE INDEX `ID_UNIQUE` (`ID` ASC),"+
		    				  "UNIQUE INDEX `PNC_UNIQUE` (`PNC` ASC),"+
		    				  " CONSTRAINT `information`"+
		    				    " FOREIGN KEY (`PNC`)"+
		    				    " REFERENCES `"+dbName+"`.`client_account` (`PNC`)"+
		    				    " ON DELETE CASCADE"+
		    				    " ON UPDATE CASCADE);";
		    Statement stmt = null;
		    try {
		      stmt = con.createStatement();
		      stmt.executeUpdate(createString);
		    } catch (SQLException e) {
		      JDBCUtilities.printSQLException(e);
		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
		  }
	  public void populateTable() throws SQLException {
	    Statement stmt = null;
	    try {
	      stmt = con.createStatement();
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 1, 'Grigore Vasile', '426433', '3482943842353','Cluj-Napoca,Jud. Cluj,nr. 45234')");
	      stmt.executeUpdate("insert into " +tableName+
	                         " values( 2, 'Alin Dumitru', '659764','3948528345204', 'Cluj-Napoca,Jud. Cluj,nr. 6345')");
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 3, 'Vladimir Ratiu', '930823','2039389231022','Cluj-Napoca,Jud. Cluj,nr. 5246')");
	     
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }

  
	 
	  
	  public void deleteRow(int id) throws SQLException {
		  Statement stmt = null;
		  String query = "DELETE FROM "+tableName+" WHERE `ID`='"+id+"'";
		    try {
		    	 stmt = con.createStatement();
		        stmt.executeUpdate(query);


		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
	  }  
	 

	 



	  public static void main(String[] args) {
	    JDBCUtilities myJDBCUtilities;
	    Connection myConnection = null;

	   
	    
	    myJDBCUtilities = new JDBCUtilities();
	     
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      ClientInformationTable table =
	        new ClientInformationTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	      System.out.println("\nDropping table:");
	      table. dropTable();
	      
	      System.out.println("\nCreating table:");
	      table.createTable();
	      System.out.println("\nPopulating table:");
	     table.populateTable();
	      System.out.println("\nContents of table:");
	      
	    
	     System.out.println(
	    		 table.getTable());
	     System.out.println(JDBCUtilities.getTable(myConnection, table.getTableName()));
	     /*
	      System.out.println("\n Viewing a employee:");
	      System.out.println(
	    		  employeeTable.findPnc(myConnection,"148324334235"));
	     
	      System.out.println("\nDeleting a row:");
	      employeeTable.deleteRow("148324334235");
	      EmployeeTable.viewTable(myConnection);
	     
	     System.out.println("\nDeleting table:");
	     employeeTable.deleteAllRows();
	     EmployeeTable.viewTable(myConnection);
	      

	      System.out.println("\nPopulating table:");
	      employeeTable.populateTable();
	      EmployeeTable.viewTable(myConnection);
	    
	      
	      
	      System.out.println("\nInserting a new row:");
	      employeeTable.insertRow("Kona Diana", "328492218432",2843.23, "2006-07-23");
	      EmployeeTable.viewTable(myConnection);
	      
	      
	      System.out.println("\nUpdating table:");
	      employeeTable.updateTable( JDBCUtilities.getTable(myConnection,TABLE_NAME));
	      EmployeeTable.viewTable(myConnection);
	      
	   /*
	      System.out.println("\nUpdating table:");
	      HashMap<Integer,ArrayList<String>> newInfo = new HashMap<Integer,ArrayList<String>>();
	      ArrayList<String> client1 = new ArrayList<String>();
	      client1.add(0, "Ana Dimiu");
	      client1.add(1, "394852809869");
	      client1.add(2, "3415.27");
	      client1.add(3, "2002-03-05");
	      newInfo.put(1,client1);
	      ArrayList<String> stud2 = new ArrayList<String>();
	      stud2.add(0, "Gashpar Ion");
	      stud2.add(1, "348294384235");
	      stud2.add(2, "9009.12");
	      stud2.add(3, "2004-05-03");
	      
	      newInfo.put(2,stud2);
	    //  System.out.println(newStudentInfo);
	      employeeTable.updateTable(newInfo);
	     EmployeeTable.viewTable(myConnection);
	     /*
	*/
	     
	      

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } catch (Exception e) {
	        e.printStackTrace(System.err);
	    } finally {
	      JDBCUtilities.closeConnection(myConnection);
	    }
	  }
	
	
}

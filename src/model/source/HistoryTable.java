package model.source;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class HistoryTable extends Table {

	public HistoryTable(Connection connArg, String dbNameArg, String dbmsArg) {
	    super();
	    this.con = connArg;
	    this.dbName = dbNameArg;
	    this.dbms = dbmsArg;
	    this.tableName="history"; 
	  }
	 public void createTable() throws SQLException {
		    String createString =
		    		"CREATE TABLE `"+dbName+"`.`"+tableName+"` ("+
		    				"`ID` INT NOT NULL AUTO_INCREMENT,"+
		    				"`Operation` VARCHAR(45) NOT NULL,"+
		    				"`UserPNC` BIGINT NOT NULL,"+
		    				"		  `clientPNC` BIGINT NULL,"+
		    				"		  `terrain_ID` INT NULL,"+
		    				" `Date` DATETIME NOT NULL,"+
		    				"	  PRIMARY KEY (`ID`));";
		    Statement stmt = null;
		    try {
		      stmt = con.createStatement();
		      stmt.executeUpdate(createString);
		    } catch (SQLException e) {
		      JDBCUtilities.printSQLException(e);
		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
		  }
	  public void populateTable() throws SQLException {
	    Statement stmt = null;
	    try {
	      stmt = con.createStatement();
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 1, 'Update client information', '2039849231025', '3482943842353',NULL,'2016-05-26 00:00:00')");
	      stmt.executeUpdate("insert into " +tableName+
	                         " values( 2, 'Update account', '2039849231025','2039389231022',NULL, '2016-05-26 00:00:00')");
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 3, 'Insert terrain information', '2039849231025',NULL,NULL,'2016-05-26 00:00:00')");
	     
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }

  
	 
	  
	  public void deleteRow(String pnc) throws SQLException {
		  Statement stmt = null;
		  String query = "DELETE FROM "+tableName+" WHERE `PNC`='"+pnc+"'";
		    try {
		    	 stmt = con.createStatement();
		        stmt.executeUpdate(query);


		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
	  }  
	 

	 



	  public static void main(String[] args) {
	    JDBCUtilities myJDBCUtilities;
	    Connection myConnection = null;

	   
	    
	    myJDBCUtilities = new JDBCUtilities();
	     
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      HistoryTable table =
	        new HistoryTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	      System.out.println("\nDropping table:");
	      table. dropTable();
	      
	      System.out.println("\nCreating table:");
	      table.createTable();
	      System.out.println("\nPopulating table:");
	    // table.populateTable();
	      System.out.println("\nContents of table:");
	      
	    
	     System.out.println(
	    		 table.getTable());
	     System.out.println(JDBCUtilities.getTable(myConnection, table.getTableName()));
	     /*
	      System.out.println("\n Viewing a employee:");
	      System.out.println(
	    		  employeeTable.findPnc(myConnection,"148324334235"));
	     
	      System.out.println("\nDeleting a row:");
	      employeeTable.deleteRow("148324334235");
	      EmployeeTable.viewTable(myConnection);
	     
	     System.out.println("\nDeleting table:");
	     employeeTable.deleteAllRows();
	     EmployeeTable.viewTable(myConnection);
	      

	      System.out.println("\nPopulating table:");
	      employeeTable.populateTable();
	      EmployeeTable.viewTable(myConnection);
	    
	      
	      
	      System.out.println("\nInserting a new row:");
	      employeeTable.insertRow("Kona Diana", "328492218432",2843.23, "2006-07-23");
	      EmployeeTable.viewTable(myConnection);
	      
	      
	      System.out.println("\nUpdating table:");
	      employeeTable.updateTable( JDBCUtilities.getTable(myConnection,TABLE_NAME));
	      EmployeeTable.viewTable(myConnection);
	      
	   /*
	      System.out.println("\nUpdating table:");
	      HashMap<Integer,ArrayList<String>> newInfo = new HashMap<Integer,ArrayList<String>>();
	      ArrayList<String> client1 = new ArrayList<String>();
	      client1.add(0, "Ana Dimiu");
	      client1.add(1, "394852809869");
	      client1.add(2, "3415.27");
	      client1.add(3, "2002-03-05");
	      newInfo.put(1,client1);
	      ArrayList<String> stud2 = new ArrayList<String>();
	      stud2.add(0, "Gashpar Ion");
	      stud2.add(1, "348294384235");
	      stud2.add(2, "9009.12");
	      stud2.add(3, "2004-05-03");
	      
	      newInfo.put(2,stud2);
	    //  System.out.println(newStudentInfo);
	      employeeTable.updateTable(newInfo);
	     EmployeeTable.viewTable(myConnection);
	     /*
	*/
	     
	      

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } catch (Exception e) {
	        e.printStackTrace(System.err);
	    } finally {
	      JDBCUtilities.closeConnection(myConnection);
	    }
	  }
	
}


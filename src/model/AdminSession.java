package model;

import java.util.ArrayList;
import java.util.HashMap;


import java.util.Set;

import model.domain.HistoryTableModule;
import model.domain.UserTableModule;

public class AdminSession{
	private String opValue;
	private String password="parola";
	private String username="username";
	UserTableModule UserModule;
	HistoryTableModule historyTable;
	private String opSearch;
	
	public AdminSession()
	{
		
		UserModule=new UserTableModule();
		//System.out.println(admin);
		//System.out.println(password);
		//System.out.println(username);
	}
	
	public String getOpSearch() {
		return opSearch;
	}

	public void setOpSearch(String opSearch) {
		this.opSearch = opSearch;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getOpValue() {
		return opValue;
	}

	public void setOpValue(String opValue) {
		this.opValue = opValue;
	}
	public HashMap<String,ArrayList<String>> printTableUsers() throws Exception
	{
		return UserModule.getTable();
	}
	
	public HashMap<String, ArrayList<String>> getHistoryTable() throws Exception {
		historyTable=new HistoryTableModule();
		return historyTable.getTable();
	}
	
	public HashMap<String,ArrayList<String>> searchUsername(String name) throws Exception
	{
		
		return UserModule.searchUserName(name);
	}
	
	public HashMap<String,ArrayList<String>> searchUserNPC(String pnc) throws Exception
	{
		return UserModule.findPnc(pnc);
	}
	public HashMap<String,ArrayList<String>> searchUserName(String name) throws Exception
	{
		return UserModule.searchName(name);
	}
	public HashMap<String,ArrayList<String>> deleteUser(String pnc) throws Exception
	{
		return UserModule.delete(pnc);
	}
	public void insertUser(HashMap<String,String> newInfo) throws Exception
	{
		UserModule.insert(newInfo);
	}
	public HashMap<String,ArrayList<String>> editUser(HashMap<String,ArrayList<String>> upInfo,HashMap<String,String> newInfo) throws Exception
	{
		return UserModule.edit(upInfo,newInfo);
	}
	public HashMap<String,ArrayList<String>> updateUser(HashMap<String,ArrayList<String>> newInfo) throws Exception
	{
		return UserModule.update(newInfo);
	}
	public Set<String> getHeader() {
		
		return UserModule.getHeader();
	}
	public static void main(String[] args) {
		
		AdminSession sess=new AdminSession();
		//admin.updateBook(new Book("0547928211", "J. Tolkien", "The Fellowship of the Ring","apidorano",12,23, "1998-09-01"));
		try {
			System.out.println(sess.printTableUsers());
			//System.out.println(sess.printTableSecretaries());
			System.out.println(sess.searchUserName("e"));
			System.out.println(sess.searchUsername("STD_DEV"));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}



	

}

package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


import javax.servlet.http.HttpServletResponse;

public interface Report {
	
	HttpServletResponse download(HttpServletResponse response,HashMap<String,ArrayList<String>> listBooks) throws IOException;
	

}

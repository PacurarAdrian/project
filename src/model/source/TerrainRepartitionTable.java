package model.source;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TerrainRepartitionTable extends Table {

	public TerrainRepartitionTable(Connection connArg, String dbNameArg, String dbmsArg) {
	    super();
	    this.con = connArg;
	    this.dbName = dbNameArg;
	    this.dbms = dbmsArg;
	    this.tableName="terrain_repartition"; 
	  }
	  public void createTable() throws SQLException {
		    String createString =
		    		"CREATE TABLE `"+dbName+"`.`"+tableName+"` ("+
		    				"`ID` INT NOT NULL AUTO_INCREMENT,"+
		    				 "  `Terrain_ID` INT NOT NULL,"+
							 " `client_ID` INT NOT NULL,"+
							 " `purchase_date` DATE NOT NULL,"+
							 " PRIMARY KEY (`ID`,`Terrain_ID`, `client_ID`),"+
							 " INDEX `fk_terrain_repartition_Terrain1_idx` (`Terrain_ID` ASC),"+
							 " INDEX `fk_terrain_repartition_client1_idx` (`client_ID` ASC),"+
							 " CONSTRAINT `fk_terrain_repartition_Terrain1`"+
							 "   FOREIGN KEY (`Terrain_ID`)"+
							  "  REFERENCES `"+dbName+"`.`Terrain` (`ID`)"+
							   " ON DELETE CASCADE"+
							   " ON UPDATE CASCADE,"+
							  " CONSTRAINT `fk_terrain_repartition_client1`"+
							   " FOREIGN KEY (`client_ID`)"+
							   " REFERENCES `"+dbName+"`.`client_information` (`ID`)"+
							   " ON DELETE CASCADE"+
							   " ON UPDATE CASCADE)";
		    Statement stmt = null;
		    try {
		      stmt = con.createStatement();
		      stmt.executeUpdate(createString);
		    } catch (SQLException e) {
		      JDBCUtilities.printSQLException(e);
		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
		  }
	  
	  public void populateTable() throws SQLException {
	    Statement stmt = null;
	    try {
	      stmt = con.createStatement();
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 1, 'Tinca Olteanu','6161524623452', 'secretary1', 'tincaritatata', '613.45', '1994-10-13')");
	      stmt.executeUpdate("insert into " +tableName+
	                         " values( 2, 'Alina Prunea','4612342634324', 'cerasela', 'afj456K', '628.84', '2005-09-17')");
	      stmt.executeUpdate("insert into  " +tableName+
	                         " values( 3, 'Kona Diana','2039849231020', 'didiana', 'passparola', '246.00', '2003-08-23')");
	     
	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } finally {
	      if (stmt != null) { stmt.close(); }
	    }
	  }
  
	 
	  
	  public void deleteRow(String id) throws SQLException {
		  Statement stmt = null;
		  String query = "DELETE FROM "+tableName+" WHERE `ID`='"+id+"'";
		    try {
		    	 stmt = con.createStatement();
		        stmt.executeUpdate(query);


		    } finally {
		      if (stmt != null) { stmt.close(); }
		    }
	  }  
	 

	 



	  public static void main(String[] args) {
	    JDBCUtilities myJDBCUtilities;
	    Connection myConnection = null;

	   
	    
	    myJDBCUtilities = new JDBCUtilities();
	     
	    try {
	      myConnection = myJDBCUtilities.getConnection();

	      TerrainRepartitionTable secretaryTable =
	        new TerrainRepartitionTable(myConnection, myJDBCUtilities.dbName,
	                         myJDBCUtilities.dbms);
	      
	      System.out.println("\nDropping table:");
	      secretaryTable. dropTable();
	      
	      System.out.println("\nCreating table:");
	      secretaryTable.createTable();
	      System.out.println("\nPopulating table:");
	    //secretaryTable.populateTable();
	      System.out.println("\nContents of table:");
	      
	    
	     System.out.println(
	    		 secretaryTable.getTable());
	     System.out.println(JDBCUtilities.getTable(myConnection, secretaryTable.getTableName()));
	     /*
	      System.out.println("\n Viewing a employee:");
	      System.out.println(
	    		  employeeTable.findPnc(myConnection,"148324334235"));
	     
	      System.out.println("\nDeleting a row:");
	      employeeTable.deleteRow("148324334235");
	      EmployeeTable.viewTable(myConnection);
	     
	     System.out.println("\nDeleting table:");
	     employeeTable.deleteAllRows();
	     EmployeeTable.viewTable(myConnection);
	      

	      System.out.println("\nPopulating table:");
	      employeeTable.populateTable();
	      EmployeeTable.viewTable(myConnection);
	    
	      
	      
	      System.out.println("\nInserting a new row:");
	      employeeTable.insertRow("Kona Diana", "328492218432",2843.23, "2006-07-23");
	      EmployeeTable.viewTable(myConnection);
	      
	      
	      System.out.println("\nUpdating table:");
	      employeeTable.updateTable( JDBCUtilities.getTable(myConnection,TABLE_NAME));
	      EmployeeTable.viewTable(myConnection);
	      
	   /*
	      System.out.println("\nUpdating table:");
	      HashMap<Integer,ArrayList<String>> newInfo = new HashMap<Integer,ArrayList<String>>();
	      ArrayList<String> client1 = new ArrayList<String>();
	      client1.add(0, "Ana Dimiu");
	      client1.add(1, "394852809869");
	      client1.add(2, "3415.27");
	      client1.add(3, "2002-03-05");
	      newInfo.put(1,client1);
	      ArrayList<String> stud2 = new ArrayList<String>();
	      stud2.add(0, "Gashpar Ion");
	      stud2.add(1, "348294384235");
	      stud2.add(2, "9009.12");
	      stud2.add(3, "2004-05-03");
	      
	      newInfo.put(2,stud2);
	    //  System.out.println(newStudentInfo);
	      employeeTable.updateTable(newInfo);
	     EmployeeTable.viewTable(myConnection);
	     /*
	*/
	     
	      

	    } catch (SQLException e) {
	      JDBCUtilities.printSQLException(e);
	    } catch (Exception e) {
	        e.printStackTrace(System.err);
	    } finally {
	      JDBCUtilities.closeConnection(myConnection);
	    }
	  }
	

}

package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import model.domain.ClientTableModule;
import model.domain.HistoryTableModule;
import model.domain.ClientAccountModule;
import model.domain.TerrainTableModule;

public class SecretarySession {
	
	private String opValue;
	private String opSearch;
	private ClientAccountModule accountModule;
	private ClientTableModule clientModule;
	private HistoryTableModule historyModule;
	private TerrainTableModule terrainModule;
	private String secretaryPnc;
	public SecretarySession()
	{
		accountModule=new ClientAccountModule();
		clientModule=new ClientTableModule();
		terrainModule=new TerrainTableModule();
	}
	
	
	 
	public String getSecretaryPnc() {
		return secretaryPnc;
	}



	public void setSecretaryPnc(String secretaryPnc) {
		this.secretaryPnc = secretaryPnc;
	}



	public String getOpValue() {
		return opValue;
	}

	public void setOpValue(String opValue) {
		this.opValue = opValue;
	}

	public String getOpSearch() {
		return opSearch;
	}

	public void setOpSearch(String opSearch) {
		this.opSearch = opSearch;
	}
	public HashMap<String,ArrayList<String>> printTableAccount() throws Exception
	{
		return accountModule.getTable();
	}
	public HashMap<String,ArrayList<String>> printTableClient() throws Exception
	{
		return clientModule.getTable();
	}
	public HashMap<String,ArrayList<String>> printTableTerrain() throws Exception
	{
		return terrainModule.getTable();
	}
	public  HashMap<String,ArrayList<String>> searchClientPnc(String pnc) throws Exception
	{
		
		return clientModule.findPnc(pnc);
	}
	public  HashMap<String,ArrayList<String>> searchTerrainPnc(String pnc) throws Exception
	{
		return terrainModule.findPnc(pnc);
	}
	public  HashMap<String,ArrayList<String>> searchAccountPnc(String pnc) throws Exception
	{
		return accountModule.findPnc(pnc);
	}
	public HashMap<String,ArrayList<String>> searchClientName(String name) throws Exception {
		
		return clientModule.findName(name);
	}
	public HashMap<String,ArrayList<String>> updateClient(HashMap<String,ArrayList<String>> newInfo) throws Exception
	{
		HashMap<String,ArrayList<String>> table=clientModule.update(newInfo);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		ArrayList<String> clientPNC=newInfo.get("PNC");
		for(String e:clientPNC)
		{
			info.clear();
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Update client information");
			info.put("clientPNC", e);
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
		}
		
		return table;
	}
	public HashMap<String,ArrayList<String>> updateTerrain(HashMap<String,ArrayList<String>> newInfo) throws Exception
	{
		HashMap<String,ArrayList<String>> table=terrainModule.update(newInfo);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		ArrayList<String> terrain=newInfo.get("ID");
		ArrayList<String> clientPNC=newInfo.get("OwnerPNC");
		int n=terrain.size();
		for(int i=0;i<n;i++)
		{
			info.clear();
			info.put("terrain_ID", terrain.get(i));
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Update terrain");
			info.put("clientPNC", clientPNC.get(i));
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
		}
		
		return table;
	}
	public HashMap<String,ArrayList<String>> updateAccount(HashMap<String,ArrayList<String>> newInfo) throws Exception
	{
		HashMap<String,ArrayList<String>> table=accountModule.update(newInfo);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		ArrayList<String> clientPNC=newInfo.get("PNC");
		int n=clientPNC.size();
		for(int i=0;i<n;i++)
		{
			info.clear();
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Update account");
			info.put("clientPNC", clientPNC.get(i));
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
		}
		
		return table;
	}
	public HashMap<String,ArrayList<String>> deleteClient(String pnc) throws Exception
	{
		HashMap<String,ArrayList<String>> table=clientModule.delete(pnc);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		
		
			
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Delete client information");
			info.put("clientPNC", pnc);
			info.put("Date", String.format("%tF", new Date()));
		historyModule.insert(info);
		return table;
	}
	public HashMap<String,ArrayList<String>> deleteClient(int id) throws Exception
	{
		HashMap<String,ArrayList<String>> table=clientModule.delete(id);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		
		
			
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Delete client information for ID "+id);
			info.put("Date", String.format("%tF", new Date()));
		
		historyModule.insert(info);
		return table;
	}
	public HashMap<String,ArrayList<String>> deleteTerrain(int id) throws Exception
	{
		HashMap<String,ArrayList<String>> table=terrainModule.delete(id);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		
		
			
		info.put("UserPNC", this.secretaryPnc);
		info.put("Operation", "Delete terrain information ");
		info.put("terrain_id", ""+id);
		info.put("Date", String.format("%tF", new Date()));
		historyModule.insert(info);
		return table;
	}
	public HashMap<String,ArrayList<String>> deleteAccount(int id) throws Exception
	{
		HashMap<String,ArrayList<String>> table=accountModule.delete(id);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		
		
			
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Delete client account for ID "+id);
			info.put("Date", String.format("%tF", new Date()));
		
		historyModule.insert(info);
		return table;
	}
	public void insertClient(HashMap<String,String> newInfo) throws Exception
	{
		
		clientModule.insert(newInfo);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		
		
			
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Insert client information");
			info.put("clientPNC", newInfo.get("PNC"));
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
		historyModule.insert(info);
	}
	
	public void insertAccount(HashMap<String,String> newInfo) throws Exception
	{
		accountModule.insert(newInfo);
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();
		
		
			
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Insert client account");
			info.put("clientPNC", newInfo.get("PNC"));
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
		
	}
	 public Set<String> getInformationHeader() throws Exception
	  {
		 return clientModule.getHeader();
	  }
	 public Set<String> getTerrainHeader() throws Exception
	  {
		 return terrainModule.getHeader();
	  }
	 public Set<String> getAccountHeader() throws Exception {
			
		 return accountModule.getHeader();
	}
	 public HashMap<String,ArrayList<String>> editClient(HashMap<String,ArrayList<String>> upInfo,HashMap<String,String> newInfo) throws Exception
	{
		 HashMap<String,ArrayList<String>> table=clientModule.edit(upInfo,newInfo);
		 
		historyModule=new HistoryTableModule();
		HashMap<String,String> info=new HashMap<String,String>();	
		info.put("UserPNC", this.secretaryPnc);
		info.put("Operation", "Insert client information");
		info.put("clientPNC", newInfo.get("PNC"));
		info.put("Date", String.format("%tF", new Date()));
		ArrayList<String> clientPNC=upInfo.get("PNC");
		historyModule.insert(info);
		for(String e:clientPNC)
		{
			info.clear();
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Update client information");
			info.put("clientPNC", e);
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
		}
		
		return table;
	}
	 public HashMap<String,ArrayList<String>> editTerrain(HashMap<String,ArrayList<String>> upInfo,HashMap<String,String> newInfo) throws Exception
	{
		 HashMap<String,ArrayList<String>> table=terrainModule.edit(upInfo,newInfo);
		 historyModule=new HistoryTableModule();
			HashMap<String,String> info=new HashMap<String,String>();	
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Insert terrain information");
			info.put("terrain_id", newInfo.get("ID"));
			info.put("Date", String.format("%tF", new Date()));
			historyModule.insert(info);
			
			ArrayList<String> ids=upInfo.get("ID");
			
			for(String e:ids)
			{
				info.clear();
				info.put("UserPNC", this.secretaryPnc);
				info.put("Operation", "Update terrain information");
				info.put("terrain_id", e);
				info.put("Date", String.format("%tF", new Date()));
				historyModule.insert(info);
			}
		return table;
	}
	 public HashMap<String,ArrayList<String>> editAccount(HashMap<String,ArrayList<String>> upInfo,HashMap<String,String> newInfo) throws Exception
	{
		 HashMap<String,ArrayList<String>> table=accountModule.edit(upInfo,newInfo);
		 historyModule=new HistoryTableModule();
			HashMap<String,String> info=new HashMap<String,String>();	
			info.put("UserPNC", this.secretaryPnc);
			info.put("Operation", "Insert client information");
			info.put("clientPNC", newInfo.get("PNC"));
			info.put("Date", String.format("%tF", new Date()));
			ArrayList<String> clientPNC=upInfo.get("PNC");
			historyModule.insert(info);
			for(String e:clientPNC)
			{
				info.clear();
				info.put("UserPNC", this.secretaryPnc);
				info.put("Operation", "Update client information");
				info.put("clientPNC", e);
				info.put("Date", String.format("%tF", new Date()));
				historyModule.insert(info);
			}
		 
		return table;
	}
	 public static void main(String[] args)
	 {
		 SecretarySession sess=new SecretarySession();
			//admin.updateBook(new Book("0547928211", "J. Tolkien", "The Fellowship of the Ring","apidorano",12,23, "1998-09-01"));
			try {
				System.out.println(sess.printTableClient());
				//System.out.println(sess.printTableSecretaries());
				
				//System.out.println(sess.printTablePatient());
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	 }



	



	

}
